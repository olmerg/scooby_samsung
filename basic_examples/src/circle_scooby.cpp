#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <utility>

#include "rclcpp/rclcpp.hpp"
//#include "std_msgs/msg/string.hpp"
#include <geometry_msgs/msg/twist.hpp>

#include <sensor_msgs/msg/laser_scan.hpp>
using std::placeholders::_1;
using namespace std::chrono_literals;

/* 
A class faz uma publicacao do topico 
para modificar o parametro fazer
ros2 param set /sooby_circle vel_scooby 2.0


subscripcao ao ros2 topic echo /scooby/sensors/lasers/rear_left_data
 */

class MinimalPublisher : public rclcpp::Node
{
  public:
    MinimalPublisher()
    : Node("sooby_circle"), count_(0)
    {
      publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("scooby/cmd_vel", 10);
      timer_ = this->create_wall_timer(500ms, std::bind(&MinimalPublisher::timer_callback, this));

      /// 3o Exercício: Subscriber para os dados do laser do robô
      auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());   
      subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "/scooby/sensors/laser_scan",  default_qos,std::bind(&MinimalPublisher::read_laser, this, _1));
      
      this->declare_parameter<double>("vel_scooby", 1.0);
    }

  private:
  /// 3o Exercício: Subscriber para os dados do laser do robô
  void read_laser(const sensor_msgs::msg::LaserScan::SharedPtr _msg){
    bool stop = false;
    for (int i = 0; i < _msg->ranges.size(); i++)
    {
      auto medicao = _msg->ranges[i];
      if(medicao<20){
        RCLCPP_INFO(this->get_logger(),"Medicao: %f",medicao);
        stop = true;
      }
    }
    auto velocity = geometry_msgs::msg::Twist();
    if (stop)
    {
      count_=1000;
      velocity.linear.x = 0.0;
      velocity.angular.z = 0.0;
    }else
    {
      count_=0;
      velocity.linear.x = 0.5;
      velocity.angular.z = 0.5;
    }
    publisher_->publish(velocity); 
  }

  void timer_callback()
  {
    auto velocity = geometry_msgs::msg::Twist();
    if (count_<200)
    {
      velocity.linear.x = 0.5;
      velocity.angular.z = 0.5;
    }else
    {
      velocity.linear.x = 0.0;
      velocity.angular.z = 0.0;
    }
    RCLCPP_INFO(this->get_logger(), "linear vel: '%.2f, angular vel: %.2f", velocity.linear.x,velocity.angular.z);
    publisher_->publish(velocity);
    count_++;
  }
  
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
  size_t count_;
};

  int main(int argc, char * argv[])
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MinimalPublisher>());
    rclcpp::shutdown();
    return 0;
  }



