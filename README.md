# Scooby the Samsung robot


Packages for launching Scooby demo, which uses Gazebo and ROS 2.

Scooby has been tested on:

* ROS 2 version:
    * ROS Foxy: `foxy` branch
* Gazebo version:
    * Gazebo 11
* Operating system:
    * Ubuntu Focal (20.04)
    * Wndows 10 with WSL2 and Docker

## Install

Install instructions for Ubuntu Focal.

1. Install the appropriate ROS 2 version as instructed [here](https://index.ros.org/doc/ros2/Installation/Linux-Install-Debians/).

2. Install `gazebo_ros_pkgs`, which also installs Gazebo. Substitute `<distro>` with `foxy` :

        sudo apt install ros-<distro>-gazebo-ros-pkgs ros-<distro>-robot-state-publisher ros-<distro>-robot-state-publisher

3. Clone ros2-serial:

        git clone https://github.com/RoverRobotics-forks/serial-ros2 

4. Clone Scooby:

        mkdir -p ~/ws/src
        cd ~/ws/src
        git clone https://olmerg@bitbucket.org/olmerg/scooby_simulation.git

5. Build and install:

        cd ~/ws
        colcon build

6. add serial conection to your user in linux (if it can not find serial port it will enter in simulation mode):

        dialout group to the user: sudo usermod -a -G dialout name_user


7. configure yaml with ports and baudrate optional (motors_node/param/scooby.yaml):

        serial:
                port1: "/dev/ttyACM0"
                port2: "/dev/ttyUSB0"
                baudrate: 115200

8. install 

	sudo apt-get install ros-foxy-joy ros-foxy-joy-linux ros-foxy-joy-teleop

9. add joystick to your user in linux

        sudo usermod -a -G input robot

10. connect the joystick an review the name of the devices with

	ls /dev/input

If the name should be *js0*, if it is different you should change in the *joy.launch.py*

## Run

1. Setup environment variables (the order is important):

        . /usr/share/gazebo/setup.sh
        . ~/ws/install/setup.bash

### Run Simulation and Teleoperation using a Virtual Joystick
1. Launch Scooby in a Samsung factory v1 (only factory, without furnitures and equipaments):

        ros2 launch scooby_gazebo scooby_default.launch.py world:=scooby_factory_v1.world

2. Launch Scooby in a Samsung factory v2:

        ros2 launch scooby_gazebo scooby_default.launch.py world:=scooby_factory_v2.world

3. Launch Scooby in an empty plant:

        ros2 launch scooby_gazebo scooby_default.launch.py world:=scooby_empty.world        
        
  this launch has the options:
  
    'world':
        SDF world file
        (default: '/home/robot/lma_ws/install/scooby_gazebo/share/scooby_gazebo/worlds/scooby_empty.world' + '')

    'use_rviz':
        Open RViz.
        (default: 'true')

    'use_robot_state_pub':
        Open robot state publisher
        (default: 'true')

    'use_teleop':
        Open scooby_teleop
        (default: 'true')

    'urdf_file':
        urddeclare_urdf_cmd =f file complete path
        (default: '/home/robot/lma_ws/install/scooby_description/share/scooby_description/urdf/scooby.urdf')

### Run nodes to control Scooby using a Real Joystick
1. Start the Simulation using one of the options listed above

2. Launch motors_node

    ros2 launch motors_node joystick.launch.py use_rviz:=True

Instruction:

- hold button 4 and control with axis 0(v_x) and axis 1(w_z) the robot

- If you release the button before the axis the command to the robot will be hold.

- To stop the robot in any time press button 0 and 2


3. Configure or review number of button/axis (Optional)
	- view number of button in the next way, running $12$ in new console:
	
		ros2 topic echo /joy
	
	```
	  frame_id: joy
		axes:
		- -0.0
		- -0.0
		- 0.0
		- -0.0
		- 0.0
		- 0.0
		- 0.0
		- 0.0
		buttons:
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		- 0
		---
	```
	
	- test each button or axes of the joystick and take notes of the number of the array which change
	- open param/joy_teleop.yaml and configure the axis and deadman_buttons which your preferences
	- save file and build again the package.

## Packages

This repository contains 7 packages:

* `scooby`: Metapackage which provides all other packages related to the Scooby robot.
* `scooby_simulations`: Metapackage which provides all other packages related to the Simulation.
* `scooby_follow`: Provides node with follow logic.
* `scooby_gazebo`: Sansung Robot model, simulation world and launch scripts.
* `scooby_description` : Contain the urdf file to visualize the robot in rviz and create the frames inside the robot
* `scooby_teleop`: Provides node to control the robot using a virtual joystick
* `motors_node`: Implements nodes to send velocity information to move the robot and provides a node to use a real joystick.

## Docker

A complete environment to run this project with Docker have been created and is available at:

https://nat-sayuri@bitbucket.org/nat-sayuri/scooby_docker.git

Follow the instructions available in the repository to add the scooby_simulation project to Docker.


