import os
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time')

    declare_use_sim_time_argument = DeclareLaunchArgument(
        'use_sim_time',
        default_value='true',
        description='Use simulation/Gazebo clock')

    start_scooby_async_slam_toolbox_node = Node(
        parameters=[
          get_package_share_directory("scooby_slamtoolbox") + '/config/scooby_mapper_params_online_async.yaml',
          {'use_sim_time': use_sim_time}
        ],
        package='slam_toolbox',
        executable='async_slam_toolbox_node',
        name='slam_toolbox',
        # remappings=[
        #     ("tf", "tf_slam")
        # ],
        output='screen')

    rviz_config_dir = os.path.join(get_package_share_directory('scooby_slamtoolbox'),
                                   'rviz', 'scooby_slamtoolbox.rviz')
    
    rviz_node = Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir],
            parameters=[{'use_sim_time': use_sim_time}],
            output='screen')             
                                   
    ld = LaunchDescription()

    ld.add_action(declare_use_sim_time_argument)
    ld.add_action(start_scooby_async_slam_toolbox_node)
    #ld.add_action(rviz_node)
    
    return ld
