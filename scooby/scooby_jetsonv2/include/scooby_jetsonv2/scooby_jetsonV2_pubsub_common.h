// Copyright 2019 The MathWorks, Inc.
// Generated 08-Oct-2021 15:48:14
#ifndef _SCOOBY_JETSONV2_PUBSUB_COMMON_
#define _SCOOBY_JETSONV2_PUBSUB_COMMON_
#include "scooby_jetsonV2_types.h"
#ifndef SET_QOS_VALUES
#define SET_QOS_VALUES(qosStruct, hist, dep, dur, rel)  \
    {                                                   \
        qosStruct.history = hist;                       \
        qosStruct.depth = dep;                          \
        qosStruct.durability = dur;                     \
        qosStruct.reliability = rel;                    \
    }
#endif
namespace ros2 {
    namespace matlab {
        // scooby_jetsonV2/Publish
        extern void create_Pub_scooby_jetsonV2_125(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern void publish_Pub_scooby_jetsonV2_125(const SL_Bus_nav_msgs_Odometry* inBus);
        // scooby_jetsonV2/Subscribe
        extern void create_Sub_scooby_jetsonV2_126(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_scooby_jetsonV2_126(SL_Bus_geometry_msgs_Twist* outBus);
    }
}
#endif // _SCOOBY_JETSONV2_PUBSUB_COMMON_
