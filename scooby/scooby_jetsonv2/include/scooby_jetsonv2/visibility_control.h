#ifndef SCOOBY_JETSONV2__VISIBILITY_CONTROL_H_
#define SCOOBY_JETSONV2__VISIBILITY_CONTROL_H_
#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define SCOOBY_JETSONV2_EXPORT __attribute__ ((dllexport))
    #define SCOOBY_JETSONV2_IMPORT __attribute__ ((dllimport))
  #else
    #define SCOOBY_JETSONV2_EXPORT __declspec(dllexport)
    #define SCOOBY_JETSONV2_IMPORT __declspec(dllimport)
  #endif
  #ifdef SCOOBY_JETSONV2_BUILDING_LIBRARY
    #define SCOOBY_JETSONV2_PUBLIC SCOOBY_JETSONV2_EXPORT
  #else
    #define SCOOBY_JETSONV2_PUBLIC SCOOBY_JETSONV2_IMPORT
  #endif
  #define SCOOBY_JETSONV2_PUBLIC_TYPE SCOOBY_JETSONV2_PUBLIC
  #define SCOOBY_JETSONV2_LOCAL
#else
  #define SCOOBY_JETSONV2_EXPORT __attribute__ ((visibility("default")))
  #define SCOOBY_JETSONV2_IMPORT
  #if __GNUC__ >= 4
    #define SCOOBY_JETSONV2_PUBLIC __attribute__ ((visibility("default")))
    #define SCOOBY_JETSONV2_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define SCOOBY_JETSONV2_PUBLIC
    #define SCOOBY_JETSONV2_LOCAL
  #endif
  #define SCOOBY_JETSONV2_PUBLIC_TYPE
#endif
#endif  // SCOOBY_JETSONV2__VISIBILITY_CONTROL_H_
// Generated 08-Oct-2021 15:48:15
 