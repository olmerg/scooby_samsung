//
// File: scooby_jetsonV2_private.h
//
// Code generated for Simulink model 'scooby_jetsonV2'.
//
// Model version                  : 3.9
// Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
// C/C++ source code generated on : Fri Oct  8 15:48:11 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Linux 64)
// Emulation hardware selection:
//    Differs from embedded hardware (ARM Compatible->ARM 10)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_scooby_jetsonV2_private_h_
#define RTW_HEADER_scooby_jetsonV2_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#endif                                 // RTW_HEADER_scooby_jetsonV2_private_h_

//
// File trailer for generated code.
//
// [EOF]
//
