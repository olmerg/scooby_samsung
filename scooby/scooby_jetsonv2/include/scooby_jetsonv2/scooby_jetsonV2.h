//
// File: scooby_jetsonV2.h
//
// Code generated for Simulink model 'scooby_jetsonV2'.
//
// Model version                  : 3.9
// Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
// C/C++ source code generated on : Fri Oct  8 15:48:11 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Linux 64)
// Emulation hardware selection:
//    Differs from embedded hardware (ARM Compatible->ARM 10)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_scooby_jetsonV2_h_
#define RTW_HEADER_scooby_jetsonV2_h_
#include <string.h>
#include <stddef.h>
#include "rtwtypes.h"
#include "scooby_jetsonV2_pubsub_common.h"
#include "DAHostLib_Network.h"
#include "scooby_jetsonV2_types.h"

// Shared type includes
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"

// Macros for accessing real-time model data structure
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

// Block signals (default storage)
struct B_scooby_jetsonV2_T {
  real_T leaf_len_data[256];
  real_T leaf_len_data_m[256];
  real_T leaf_len_data_c[256];
  real_T temp_data[256];
  real_T leaf_len_data_k[256];
  real_T leaf_len_data_cx[256];
  int32_T temp_data_b[512];
  real_T b_leaf_len_data[256];
  uint32_T b_temp_data[512];
  real_T leaf_len_data_p[256];
  real_T leaf_len_data_cv[256];
  real_T leaf_len_data_f[256];
  uint32_T temp_data_g[512];
  uint8_T u0[2048];
  uint8_T u0_data[2048];
  uint8_T temp_data_g1[2048];
  uint8_T x_data[2048];
  uint8_T x_data_m[2048];
  uint8_T x_data_n[2048];
  uint8_T x_data_p[2048];
  uint8_T temp_data_l[2048];
  uint8_T x_data_j[2048];
  uint8_T x_data_d[2048];
  uint8_T x_data_g[2048];
  uint8_T x_data_l[2048];
  uint8_T x_data_dh[2048];
  SL_Bus_nav_msgs_Odometry ros2deserialize;// '<S3>/ros2deserialize'
  uint8_T UDPReceive2_o1[2048];        // '<Root>/UDP Receive2'
  uint8_T ros2serialize2[2048];        // '<S5>/ros2serialize2'
  SL_Bus_geometry_msgs_Twist b_varargout_2;
  uint8_T new_vec[8];
  real_T jj;
  real_T i;
  real_T b_jj;
  real_T e_tmp;
  real_T k_tmp;
  real_T e_tmp_d;
  real_T k_tmp_l;
  real_T jj_o;
  real_T g;
  real_T b_jj_b;
  real_T e_tmp_n;
  int32_T samplesRead;
  int32_T loop_ub;
};

// Block states (default storage) for system '<Root>'
struct DW_scooby_jetsonV2_T {
  ros_slros2_internal_block_Pub_T obj; // '<S4>/SinkBlock'
  ros_slros2_internal_block_Sub_T obj_n;// '<S6>/SourceBlock'
  real_T UDPReceive2_NetworkLib[137];  // '<Root>/UDP Receive2'
  real_T UDPSend_NetworkLib[137];      // '<S5>/UDP Send'
  ros2serialize_scooby_jetsonV2_T obj_b;// '<S5>/ros2serialize2'
};

// Parameters (default storage)
struct P_scooby_jetsonV2_T_ {
  int32_T UDPReceive2_localPort;       // Mask Parameter: UDPReceive2_localPort
                                          //  Referenced by: '<Root>/UDP Receive2'

  int32_T UDPSend_remotePort;          // Mask Parameter: UDPSend_remotePort
                                          //  Referenced by: '<S5>/UDP Send'

  SL_Bus_nav_msgs_Odometry Msg_Y0;     // Computed Parameter: Msg_Y0
                                          //  Referenced by: '<S3>/Msg'

  SL_Bus_nav_msgs_Odometry Constant_Value;// Computed Parameter: Constant_Value
                                             //  Referenced by: '<S1>/Constant'

  SL_Bus_geometry_msgs_Twist Out1_Y0;  // Computed Parameter: Out1_Y0
                                          //  Referenced by: '<S7>/Out1'

  SL_Bus_geometry_msgs_Twist Constant_Value_p;// Computed Parameter: Constant_Value_p
                                                 //  Referenced by: '<S6>/Constant'

  uint16_T Constant_Value_f;           // Computed Parameter: Constant_Value_f
                                          //  Referenced by: '<S2>/Constant'

};

// Real-time Model Data Structure
struct tag_RTM_scooby_jetsonV2_T {
  const char_T *errorStatus;

  //
  //  Timing:
  //  The following substructure contains information regarding
  //  the timing information for the model.

  struct {
    boolean_T stopRequestedFlag;
  } Timing;
};

// Class declaration for model scooby_jetsonV2
class scooby_jetsonV2ModelClass {
  // public data and function members
 public:
  // model initialize function
  void initialize();

  // model step function
  void step();

  // model terminate function
  void terminate();

  // Constructor
  scooby_jetsonV2ModelClass();

  // Destructor
  ~scooby_jetsonV2ModelClass();

  // Real-Time Model get method
  RT_MODEL_scooby_jetsonV2_T * getRTM();

  // private data and function members
 private:
  // Tunable parameters
  static P_scooby_jetsonV2_T scooby_jetsonV2_P;

  // Block signals
  B_scooby_jetsonV2_T scooby_jetsonV2_B;

  // Block states
  DW_scooby_jetsonV2_T scooby_jetsonV2_DW;

  // Real-Time Model
  RT_MODEL_scooby_jetsonV2_T scooby_jetsonV2_M;

  // private member function(s) for subsystem '<Root>'
  void scooby_jetsonV2_inject_data_j(const uint8_T inarr_data[], int32_T
    *msg_out_sec, uint32_T *msg_out_nanosec, real_T *depth_out);
  void scooby_jetsonV2_inject_data_j4(const uint8_T inarr_data[], real_T jj,
    uint32_T *msg_out_CurrentLength, uint32_T *msg_out_ReceivedLength, real_T
    *depth_out);
  void scooby_jetsonV2_inject_data(const uint8_T inarr_data[], int32_T
    *msg_out_stamp_sec, uint32_T *msg_out_stamp_nanosec, uint8_T
    msg_out_frame_id[128], uint32_T *msg_out_frame_id_SL_Info_Curren, uint32_T
    *msg_out_frame_id_SL_Info_Receiv, real_T *depth_out);
  void scooby_jetson_inject_data_j4t4l(const uint8_T inarr_data[], real_T jj,
    real_T *msg_out_x, real_T *msg_out_y, real_T *msg_out_z, real_T *msg_out_w,
    real_T *depth_out);
  void scooby_jetsonV_inject_data_j4t4(const uint8_T inarr_data[], real_T jj,
    real_T *msg_out_position_x, real_T *msg_out_position_y, real_T
    *msg_out_position_z, real_T *msg_out_orientation_x, real_T
    *msg_out_orientation_y, real_T *msg_out_orientation_z, real_T
    *msg_out_orientation_w, real_T *depth_out);
  void scooby_jetsonV2_inject_data_j4t(const uint8_T inarr_data[], real_T jj,
    real_T *msg_out_pose_position_x, real_T *msg_out_pose_position_y, real_T
    *msg_out_pose_position_z, SL_Bus_geometry_msgs_Quaternion
    *msg_out_pose_orientation, real_T msg_out_covariance[36], real_T *depth_out);
  void scooby_jets_inject_data_j4t4lg3(const uint8_T inarr_data[], real_T jj,
    real_T *msg_out_x, real_T *msg_out_y, real_T *msg_out_z, real_T *depth_out);
  void scooby_jetso_inject_data_j4t4lg(const uint8_T inarr_data[], real_T jj,
    real_T *msg_out_twist_linear_x, real_T *msg_out_twist_linear_y, real_T
    *msg_out_twist_linear_z, real_T *msg_out_twist_angular_x, real_T
    *msg_out_twist_angular_y, real_T *msg_out_twist_angular_z, real_T
    msg_out_covariance[36], real_T *depth_out);
  void scooby_jetsonV2_rosmsgUnpack(const uint8_T arr_data[],
    SL_Bus_builtin_interfaces_Time *msg_out_header_stamp, uint8_T
    msg_out_header_frame_id[128], SL_Bus_ROSVariableLengthArrayInfo
    *msg_out_header_frame_id_SL_Info, uint8_T msg_out_child_frame_id[128],
    SL_Bus_ROSVariableLengthArrayInfo *msg_out_child_frame_id_SL_Info,
    SL_Bus_geometry_msgs_PoseWithCovariance *msg_out_pose,
    SL_Bus_geometry_msgs_TwistWithCovariance *msg_out_twist);
  void scooby_je_rosmsgSerializeStruct(real_T msg_x, real_T msg_y, real_T msg_z,
    uint8_T y[2048]);
  void scooby_j_ros2serialize_stepImpl(real_T Msg_linear_x, real_T Msg_linear_y,
    real_T Msg_linear_z, real_T Msg_angular_x, real_T Msg_angular_y, real_T
    Msg_angular_z, uint8_T Data[2048]);
  void scooby_jetsonV_SystemCore_setup(ros_slros2_internal_block_Pub_T *obj);
  void scooby_jetso_SystemCore_setup_j(ros_slros2_internal_block_Sub_T *obj);
};

extern volatile boolean_T stopRequested;
extern volatile boolean_T runModel;

//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'scooby_jetsonV2'
//  '<S1>'   : 'scooby_jetsonV2/Blank Message3'
//  '<S2>'   : 'scooby_jetsonV2/Compare To Zero1'
//  '<S3>'   : 'scooby_jetsonV2/Deserialize1'
//  '<S4>'   : 'scooby_jetsonV2/Publish'
//  '<S5>'   : 'scooby_jetsonV2/Send Message over UDP'
//  '<S6>'   : 'scooby_jetsonV2/Subscribe'
//  '<S7>'   : 'scooby_jetsonV2/Subscribe/Enabled Subsystem'

#endif                                 // RTW_HEADER_scooby_jetsonV2_h_

//
// File trailer for generated code.
//
// [EOF]
//
