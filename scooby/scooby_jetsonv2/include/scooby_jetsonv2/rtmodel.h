//
// File: rtmodel.h
//
// Code generated for Simulink model 'scooby_jetsonV2'.
//
// Model version                  : 3.9
// Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
// C/C++ source code generated on : Fri Oct  8 15:48:11 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Linux 64)
// Emulation hardware selection:
//    Differs from embedded hardware (ARM Compatible->ARM 10)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_
#include "scooby_jetsonV2.h"
#define MODEL_CLASSNAME                scooby_jetsonV2ModelClass
#define MODEL_STEPNAME                 step

//
//  ROOT_IO_FORMAT: 0 (Individual arguments)
//  ROOT_IO_FORMAT: 1 (Structure reference)
//  ROOT_IO_FORMAT: 2 (Part of model data structure)

#define ROOT_IO_FORMAT                 1
#endif                                 // RTW_HEADER_rtmodel_h_

//
// File trailer for generated code.
//
// [EOF]
//
