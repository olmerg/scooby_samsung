// Copyright 2019-2021 The MathWorks, Inc.
// Generated 08-Oct-2021 15:48:14
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244)
#pragma warning(disable : 4265)
#pragma warning(disable : 4458)
#pragma warning(disable : 4100)
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wshadow"
#endif //_MSC_VER
#include "rclcpp/rclcpp.hpp"
#include "scooby_jetsonV2.h"
#include "ros2nodeinterface.h"
#include <thread>
#include <chrono>
#include <utility>
const std::string SLROSNodeName("scooby_jetsonV2");
namespace ros2 {
namespace matlab {
NodeInterface::NodeInterface()
    : mNode()
    , mModel()
    , mExec()
    , mBaseRateSem()
    , mBaseRateThread()
    , mSchedulerThread()
    , mStopSem()
    , mRunModel(true){
  }
NodeInterface::~NodeInterface() {
    terminate();
  }
void NodeInterface::initialize(int argc, char * const argv[]) {
    try {
        //initialize ros2
        // Workaround to disable /rosout topic until rmw_fastrtps supports
        // multiple typesupport implementations for the same topic (https://github.com/ros2/rmw_fastrtps/issues/265)
        std::vector<char *> args(argv, argv + argc);
        char log_disable_rosout[] = "__log_disable_rosout:=true";
        args.push_back(log_disable_rosout);
        rclcpp::init(static_cast<int>(args.size()), args.data());
        //create the Node specified in Model
        std::string NodeName("scooby_jetsonV2");
        mNode = std::make_shared<rclcpp::Node>(NodeName);
        mExec = std::make_shared<rclcpp::executors::MultiThreadedExecutor>();
        mExec->add_node(mNode);
        //initialize the model which will initialize the publishers and subscribers
        mModel = std::make_shared<scooby_jetsonV2ModelClass>();
		rtmSetErrorStatus(mModel->getRTM(), (NULL));
        mModel->initialize();
        //create the threads for the rates in the Model
        mBaseRateThread = std::make_shared<std::thread>(&NodeInterface::baseRateTask, this);
		mSchedulerThread = std::make_shared<std::thread>(&NodeInterface::schedulerThreadCallback, this);
    }
    catch (std::exception& ex) {
        std::cout << ex.what() << std::endl;
        throw ex;
    }
    catch (...) {
        std::cout << "Unknown exception" << std::endl;
        throw;
    }
}
int NodeInterface::run() {
  if (mExec) {
    mExec->spin();
  }
  mRunModel = false;
  return 0;
}
boolean_T NodeInterface::getStopRequestedFlag(void) {
    #ifndef rtmGetStopRequested
    return (!(rtmGetErrorStatus(mModel->getRTM())
        == (NULL)));
    #else
    return (!(rtmGetErrorStatus(mModel->getRTM())
        == (NULL)) || rtmGetStopRequested(mModel->getRTM()));
    #endif
}
void NodeInterface::stop(void) {
  if (mExec.get()) {
    mExec->cancel();
    if (mNode) {
      mExec->remove_node(mNode);
    }
    while (mExec.use_count() > 1);
  }
}
void NodeInterface::terminate(void) {
    if (mBaseRateThread.get()) {
        mRunModel = false;
        mBaseRateSem.notify(); // break out wait
        mBaseRateThread->join();
        if (mSchedulerThread.get()) {
            mSchedulerThread->join();
            mSchedulerThread.reset();
        }
        mBaseRateThread.reset();
        if (mModel.get()) {
            mModel->terminate();
        }
        // Release publisher scooby_jetsonV2/Publish
        mPub_scooby_jetsonV2_125.reset();
        // Release subscriber scooby_jetsonV2/Subscribe
        mSub_scooby_jetsonV2_126.reset();
        mModel.reset();
        mExec.reset();
        mNode.reset();
        rclcpp::shutdown();
    }
}
//
void NodeInterface::schedulerThreadCallback(void)
{
  while (mRunModel) {
        std::this_thread::sleep_until(std::chrono::system_clock::now() + std::chrono::nanoseconds(50000000));
        mBaseRateSem.notify();
    }
}
//Model specific
void NodeInterface::baseRateTask(void) {
  mRunModel = (rtmGetErrorStatus(mModel->getRTM()) ==
              (NULL));
  while (mRunModel) {
    mBaseRateSem.wait();
    if (!mRunModel) break;
    mModel->step();
    mRunModel &= !NodeInterface::getStopRequestedFlag(); //If RunModel and not stop requested
  }
  NodeInterface::stop();
}
// scooby_jetsonV2/Publish
void NodeInterface::create_Pub_scooby_jetsonV2_125(const char *topicName, const rmw_qos_profile_t& qosProfile){
  mPub_scooby_jetsonV2_125 = mNode->create_publisher<nav_msgs::msg::Odometry>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile));
}
void NodeInterface::publish_Pub_scooby_jetsonV2_125(const SL_Bus_nav_msgs_Odometry* inBus) {
  auto msg = std::make_unique<nav_msgs::msg::Odometry>();
  convertFromBus(*msg, inBus);
  mPub_scooby_jetsonV2_125->publish(std::move(msg));
}
// scooby_jetsonV2/Subscribe
void NodeInterface::create_Sub_scooby_jetsonV2_126(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](geometry_msgs::msg::Twist::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_scooby_jetsonV2_126);
        mLatestMsg_Sub_scooby_jetsonV2_126 = msg;
    };
    mSub_scooby_jetsonV2_126 = mNode->create_subscription<geometry_msgs::msg::Twist>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_scooby_jetsonV2_126(SL_Bus_geometry_msgs_Twist* outBus) {
    if (mLatestMsg_Sub_scooby_jetsonV2_126.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_scooby_jetsonV2_126);
        convertToBus(outBus, *mLatestMsg_Sub_scooby_jetsonV2_126);
        return true;
    }
    return false;
}
// Helper for scooby_jetsonV2/Publish
void create_Pub_scooby_jetsonV2_125(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Pub_scooby_jetsonV2_125(topicName, qosProfile);
}
void publish_Pub_scooby_jetsonV2_125(const SL_Bus_nav_msgs_Odometry* inBus) {
  ros2::matlab::getNodeInterface()->publish_Pub_scooby_jetsonV2_125(inBus);
}
// Helper for scooby_jetsonV2/Subscribe
void create_Sub_scooby_jetsonV2_126(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_scooby_jetsonV2_126(topicName, qosProfile);
}
bool getLatestMessage_Sub_scooby_jetsonV2_126(SL_Bus_geometry_msgs_Twist* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_scooby_jetsonV2_126(outBus);
}
}//namespace matlab
}//namespace ros2
#ifdef _MSC_VER
#pragma warning(pop)
#else
#pragma GCC diagnostic pop
#endif //_MSC_VER
