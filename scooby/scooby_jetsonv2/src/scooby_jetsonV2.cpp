//
// File: scooby_jetsonV2.cpp
//
// Code generated for Simulink model 'scooby_jetsonV2'.
//
// Model version                  : 3.9
// Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
// C/C++ source code generated on : Fri Oct  8 15:48:11 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Linux 64)
// Emulation hardware selection:
//    Differs from embedded hardware (ARM Compatible->ARM 10)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "scooby_jetsonV2.h"
#include "scooby_jetsonV2_private.h"

void scooby_jetsonV2ModelClass::scooby_jetsonV2_inject_data_j(const uint8_T
  inarr_data[], int32_T *msg_out_sec, uint32_T *msg_out_nanosec, real_T
  *depth_out)
{
  real_T leaf_len;
  int32_T d;
  int32_T i;
  int32_T ny;
  memcpy((void *)&leaf_len, (void *)&inarr_data[0], (uint32_T)((size_t)1 *
          sizeof(real_T)));
  if (9.0 > (leaf_len + 9.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = 8;
    ny = static_cast<int32_T>((leaf_len + 9.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_d[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 2;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data_b[0], (void *)
         &scooby_jetsonV2_B.x_data_d[0], (uint32_T)((size_t)ny * sizeof(int32_T)));
  if (leaf_len + 9.0 > ((leaf_len + 9.0) + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(leaf_len + 9.0) - 1;
    ny = static_cast<int32_T>(((leaf_len + 9.0) + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_d[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.b_leaf_len_data[0], (void *)
         &scooby_jetsonV2_B.x_data_d[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = ((leaf_len + 9.0) + 8.0) + scooby_jetsonV2_B.b_leaf_len_data[0];
  if ((leaf_len + 9.0) + 8.0 > *depth_out - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>((leaf_len + 9.0) + 8.0) - 1;
    ny = static_cast<int32_T>(*depth_out - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_d[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 2;
  }

  memcpy((void *)&scooby_jetsonV2_B.b_temp_data[0], (void *)
         &scooby_jetsonV2_B.x_data_d[0], (uint32_T)((size_t)ny * sizeof(uint32_T)));
  *msg_out_sec = scooby_jetsonV2_B.temp_data_b[0];
  *msg_out_nanosec = scooby_jetsonV2_B.b_temp_data[0];
}

void scooby_jetsonV2ModelClass::scooby_jetsonV2_inject_data_j4(const uint8_T
  inarr_data[], real_T jj, uint32_T *msg_out_CurrentLength, uint32_T
  *msg_out_ReceivedLength, real_T *depth_out)
{
  real_T e_tmp;
  int32_T d;
  int32_T i;
  int32_T ny;
  if (jj > (jj + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>((jj + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_dh[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_f[0], (void *)
         &scooby_jetsonV2_B.x_data_dh[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  jj += 8.0;
  e_tmp = jj + scooby_jetsonV2_B.leaf_len_data_f[0];
  if (jj > e_tmp - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>(e_tmp - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_dh[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 2;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data_g[0], (void *)
         &scooby_jetsonV2_B.x_data_dh[0], (uint32_T)((size_t)ny * sizeof
          (uint32_T)));
  *msg_out_CurrentLength = scooby_jetsonV2_B.temp_data_g[0];
  if (e_tmp > (e_tmp + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(e_tmp) - 1;
    ny = static_cast<int32_T>((e_tmp + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_dh[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_f[0], (void *)
         &scooby_jetsonV2_B.x_data_dh[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = (e_tmp + 8.0) + scooby_jetsonV2_B.leaf_len_data_f[0];
  if (e_tmp + 8.0 > *depth_out - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(e_tmp + 8.0) - 1;
    ny = static_cast<int32_T>(*depth_out - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_dh[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 2;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data_g[0], (void *)
         &scooby_jetsonV2_B.x_data_dh[0], (uint32_T)((size_t)ny * sizeof
          (uint32_T)));
  *msg_out_ReceivedLength = scooby_jetsonV2_B.temp_data_g[0];
}

void scooby_jetsonV2ModelClass::scooby_jetsonV2_inject_data(const uint8_T
  inarr_data[], int32_T *msg_out_stamp_sec, uint32_T *msg_out_stamp_nanosec,
  uint8_T msg_out_frame_id[128], uint32_T *msg_out_frame_id_SL_Info_Curren,
  uint32_T *msg_out_frame_id_SL_Info_Receiv, real_T *depth_out)
{
  int32_T f;
  int32_T h;
  int32_T i;
  int32_T ny;
  scooby_jetsonV2_inject_data_j(inarr_data, msg_out_stamp_sec,
    msg_out_stamp_nanosec, &scooby_jetsonV2_B.jj_o);
  if (scooby_jetsonV2_B.jj_o > (scooby_jetsonV2_B.jj_o + 8.0) - 1.0) {
    f = 0;
    ny = 0;
  } else {
    f = static_cast<int32_T>(scooby_jetsonV2_B.jj_o) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.jj_o + 8.0) - 1.0);
  }

  ny -= f;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_j[i] = inarr_data[f + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cx[0], (void *)
         &scooby_jetsonV2_B.x_data_j[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.g = ((scooby_jetsonV2_B.jj_o + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_cx[0]) - 1.0;
  if (scooby_jetsonV2_B.jj_o + 8.0 > scooby_jetsonV2_B.g) {
    f = 0;
    h = 0;
  } else {
    f = static_cast<int32_T>(scooby_jetsonV2_B.jj_o + 8.0) - 1;
    h = static_cast<int32_T>(scooby_jetsonV2_B.g);
  }

  ny = h - f;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_j[i] = inarr_data[f + i];
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data_l[0], (void *)
         &scooby_jetsonV2_B.x_data_j[0], (uint32_T)((size_t)(h - f) * sizeof
          (uint8_T)));
  scooby_jetsonV2_inject_data_j4(inarr_data, (scooby_jetsonV2_B.jj_o + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_cx[0], msg_out_frame_id_SL_Info_Curren,
    msg_out_frame_id_SL_Info_Receiv, depth_out);
  memcpy(&msg_out_frame_id[0], &scooby_jetsonV2_B.temp_data_l[0], sizeof(uint8_T)
         << 7U);
}

void scooby_jetsonV2ModelClass::scooby_jetson_inject_data_j4t4l(const uint8_T
  inarr_data[], real_T jj, real_T *msg_out_x, real_T *msg_out_y, real_T
  *msg_out_z, real_T *msg_out_w, real_T *depth_out)
{
  int32_T d;
  int32_T i;
  int32_T ny;
  if (jj > (jj + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>((jj + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  jj += 8.0;
  scooby_jetsonV2_B.e_tmp_d = jj + scooby_jetsonV2_B.leaf_len_data_k[0];
  if (jj > scooby_jetsonV2_B.e_tmp_d - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_x = scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.e_tmp_d > (scooby_jetsonV2_B.e_tmp_d + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.e_tmp_d + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.k_tmp_l = (scooby_jetsonV2_B.e_tmp_d + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.e_tmp_d + 8.0 > scooby_jetsonV2_B.k_tmp_l - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d + 8.0) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp_l - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_y = scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.k_tmp_l > (scooby_jetsonV2_B.k_tmp_l + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp_l) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.k_tmp_l + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.e_tmp_d = (scooby_jetsonV2_B.k_tmp_l + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.k_tmp_l + 8.0 > scooby_jetsonV2_B.e_tmp_d - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp_l + 8.0) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_z = scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.e_tmp_d > (scooby_jetsonV2_B.e_tmp_d + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.e_tmp_d + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = (scooby_jetsonV2_B.e_tmp_d + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_k[0];
  if (scooby_jetsonV2_B.e_tmp_d + 8.0 > *depth_out - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_d + 8.0) - 1;
    ny = static_cast<int32_T>(*depth_out - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_p[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_k[0], (void *)
         &scooby_jetsonV2_B.x_data_p[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_w = scooby_jetsonV2_B.leaf_len_data_k[0];
}

void scooby_jetsonV2ModelClass::scooby_jetsonV_inject_data_j4t4(const uint8_T
  inarr_data[], real_T jj, real_T *msg_out_position_x, real_T
  *msg_out_position_y, real_T *msg_out_position_z, real_T *msg_out_orientation_x,
  real_T *msg_out_orientation_y, real_T *msg_out_orientation_z, real_T
  *msg_out_orientation_w, real_T *depth_out)
{
  int32_T d;
  int32_T i;
  int32_T ny;
  if (jj > (jj + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>((jj + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_c[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.e_tmp = (jj + 8.0) + scooby_jetsonV2_B.leaf_len_data_c[0];
  if (jj + 8.0 > scooby_jetsonV2_B.e_tmp - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj + 8.0) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_position_x = scooby_jetsonV2_B.temp_data[0];
  if (scooby_jetsonV2_B.e_tmp > (scooby_jetsonV2_B.e_tmp + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.e_tmp + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_c[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.k_tmp = (scooby_jetsonV2_B.e_tmp + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_c[0];
  if (scooby_jetsonV2_B.e_tmp + 8.0 > scooby_jetsonV2_B.k_tmp - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp + 8.0) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_position_y = scooby_jetsonV2_B.temp_data[0];
  if (scooby_jetsonV2_B.k_tmp > (scooby_jetsonV2_B.k_tmp + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.k_tmp + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_c[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.e_tmp = ((scooby_jetsonV2_B.k_tmp + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_c[0]) - 1.0;
  if (scooby_jetsonV2_B.k_tmp + 8.0 > scooby_jetsonV2_B.e_tmp) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.k_tmp + 8.0) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_n[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data[0], (void *)
         &scooby_jetsonV2_B.x_data_n[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetson_inject_data_j4t4l(inarr_data, (scooby_jetsonV2_B.k_tmp + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_c[0], msg_out_orientation_x,
    msg_out_orientation_y, msg_out_orientation_z, msg_out_orientation_w,
    depth_out);
  *msg_out_position_z = scooby_jetsonV2_B.temp_data[0];
}

void scooby_jetsonV2ModelClass::scooby_jetsonV2_inject_data_j4t(const uint8_T
  inarr_data[], real_T jj, real_T *msg_out_pose_position_x, real_T
  *msg_out_pose_position_y, real_T *msg_out_pose_position_z,
  SL_Bus_geometry_msgs_Quaternion *msg_out_pose_orientation, real_T
  msg_out_covariance[36], real_T *depth_out)
{
  int32_T d;
  int32_T i;
  int32_T ny;
  scooby_jetsonV_inject_data_j4t4(inarr_data, jj, msg_out_pose_position_x,
    msg_out_pose_position_y, msg_out_pose_position_z,
    &msg_out_pose_orientation->x, &msg_out_pose_orientation->y,
    &msg_out_pose_orientation->z, &msg_out_pose_orientation->w,
    &scooby_jetsonV2_B.b_jj);
  if (scooby_jetsonV2_B.b_jj > (scooby_jetsonV2_B.b_jj + 8.0) - 1.0) {
    ny = 0;
    d = 0;
  } else {
    ny = static_cast<int32_T>(scooby_jetsonV2_B.b_jj) - 1;
    d = static_cast<int32_T>((scooby_jetsonV2_B.b_jj + 8.0) - 1.0);
  }

  d -= ny;
  for (i = 0; i < d; i++) {
    scooby_jetsonV2_B.x_data_m[i] = inarr_data[ny + i];
  }

  if (d == 0) {
    ny = 0;
  } else {
    ny = d >> 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_m[0], (void *)
         &scooby_jetsonV2_B.x_data_m[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = (scooby_jetsonV2_B.b_jj + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_m[0];
  if (scooby_jetsonV2_B.b_jj + 8.0 > *depth_out - 1.0) {
    ny = 0;
    d = 0;
  } else {
    ny = static_cast<int32_T>(scooby_jetsonV2_B.b_jj + 8.0) - 1;
    d = static_cast<int32_T>(*depth_out - 1.0);
  }

  d -= ny;
  for (i = 0; i < d; i++) {
    scooby_jetsonV2_B.x_data_m[i] = inarr_data[ny + i];
  }

  if (d == 0) {
    ny = 0;
  } else {
    ny = d >> 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_m[0], (void *)
         &scooby_jetsonV2_B.x_data_m[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  memcpy(&msg_out_covariance[0], &scooby_jetsonV2_B.leaf_len_data_m[0], 36U *
         sizeof(real_T));
}

void scooby_jetsonV2ModelClass::scooby_jets_inject_data_j4t4lg3(const uint8_T
  inarr_data[], real_T jj, real_T *msg_out_x, real_T *msg_out_y, real_T
  *msg_out_z, real_T *depth_out)
{
  real_T k_tmp;
  int32_T d;
  int32_T i;
  int32_T ny;
  if (jj > (jj + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>((jj + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  jj += 8.0;
  scooby_jetsonV2_B.e_tmp_n = jj + scooby_jetsonV2_B.leaf_len_data_cv[0];
  if (jj > scooby_jetsonV2_B.e_tmp_n - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(jj) - 1;
    ny = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_n - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_x = scooby_jetsonV2_B.leaf_len_data_cv[0];
  if (scooby_jetsonV2_B.e_tmp_n > (scooby_jetsonV2_B.e_tmp_n + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_n) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.e_tmp_n + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  k_tmp = (scooby_jetsonV2_B.e_tmp_n + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_cv[0];
  if (scooby_jetsonV2_B.e_tmp_n + 8.0 > k_tmp - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(scooby_jetsonV2_B.e_tmp_n + 8.0) - 1;
    ny = static_cast<int32_T>(k_tmp - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_y = scooby_jetsonV2_B.leaf_len_data_cv[0];
  if (k_tmp > (k_tmp + 8.0) - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(k_tmp) - 1;
    ny = static_cast<int32_T>((k_tmp + 8.0) - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = (k_tmp + 8.0) + scooby_jetsonV2_B.leaf_len_data_cv[0];
  if (k_tmp + 8.0 > *depth_out - 1.0) {
    d = 0;
    ny = 0;
  } else {
    d = static_cast<int32_T>(k_tmp + 8.0) - 1;
    ny = static_cast<int32_T>(*depth_out - 1.0);
  }

  ny -= d;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data_l[i] = inarr_data[d + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_cv[0], (void *)
         &scooby_jetsonV2_B.x_data_l[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *msg_out_z = scooby_jetsonV2_B.leaf_len_data_cv[0];
}

void scooby_jetsonV2ModelClass::scooby_jetso_inject_data_j4t4lg(const uint8_T
  inarr_data[], real_T jj, real_T *msg_out_twist_linear_x, real_T
  *msg_out_twist_linear_y, real_T *msg_out_twist_linear_z, real_T
  *msg_out_twist_angular_x, real_T *msg_out_twist_angular_y, real_T
  *msg_out_twist_angular_z, real_T msg_out_covariance[36], real_T *depth_out)
{
  int32_T d;
  int32_T i;
  int32_T ny;
  scooby_jets_inject_data_j4t4lg3(inarr_data, jj, msg_out_twist_linear_x,
    msg_out_twist_linear_y, msg_out_twist_linear_z, &scooby_jetsonV2_B.b_jj_b);
  scooby_jets_inject_data_j4t4lg3(inarr_data, scooby_jetsonV2_B.b_jj_b,
    msg_out_twist_angular_x, msg_out_twist_angular_y, msg_out_twist_angular_z,
    &scooby_jetsonV2_B.b_jj_b);
  if (scooby_jetsonV2_B.b_jj_b > (scooby_jetsonV2_B.b_jj_b + 8.0) - 1.0) {
    ny = 0;
    d = 0;
  } else {
    ny = static_cast<int32_T>(scooby_jetsonV2_B.b_jj_b) - 1;
    d = static_cast<int32_T>((scooby_jetsonV2_B.b_jj_b + 8.0) - 1.0);
  }

  d -= ny;
  for (i = 0; i < d; i++) {
    scooby_jetsonV2_B.x_data_g[i] = inarr_data[ny + i];
  }

  if (d == 0) {
    ny = 0;
  } else {
    ny = d >> 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_p[0], (void *)
         &scooby_jetsonV2_B.x_data_g[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  *depth_out = (scooby_jetsonV2_B.b_jj_b + 8.0) +
    scooby_jetsonV2_B.leaf_len_data_p[0];
  if (scooby_jetsonV2_B.b_jj_b + 8.0 > *depth_out - 1.0) {
    ny = 0;
    d = 0;
  } else {
    ny = static_cast<int32_T>(scooby_jetsonV2_B.b_jj_b + 8.0) - 1;
    d = static_cast<int32_T>(*depth_out - 1.0);
  }

  d -= ny;
  for (i = 0; i < d; i++) {
    scooby_jetsonV2_B.x_data_g[i] = inarr_data[ny + i];
  }

  if (d == 0) {
    ny = 0;
  } else {
    ny = d >> 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data_p[0], (void *)
         &scooby_jetsonV2_B.x_data_g[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  memcpy(&msg_out_covariance[0], &scooby_jetsonV2_B.leaf_len_data_p[0], 36U *
         sizeof(real_T));
}

void scooby_jetsonV2ModelClass::scooby_jetsonV2_rosmsgUnpack(const uint8_T
  arr_data[], SL_Bus_builtin_interfaces_Time *msg_out_header_stamp, uint8_T
  msg_out_header_frame_id[128], SL_Bus_ROSVariableLengthArrayInfo
  *msg_out_header_frame_id_SL_Info, uint8_T msg_out_child_frame_id[128],
  SL_Bus_ROSVariableLengthArrayInfo *msg_out_child_frame_id_SL_Info,
  SL_Bus_geometry_msgs_PoseWithCovariance *msg_out_pose,
  SL_Bus_geometry_msgs_TwistWithCovariance *msg_out_twist)
{
  int32_T h;
  int32_T i;
  int32_T j;
  int32_T ny;

  // rosmsgUnpack Populate a structure based on a byte array
  //    Copyright 2017 The MathWorks, Inc.
  scooby_jetsonV2_inject_data(arr_data, &msg_out_header_stamp->sec,
    &msg_out_header_stamp->nanosec, msg_out_header_frame_id,
    &msg_out_header_frame_id_SL_Info->CurrentLength,
    &msg_out_header_frame_id_SL_Info->ReceivedLength, &scooby_jetsonV2_B.jj);
  if (scooby_jetsonV2_B.jj > (scooby_jetsonV2_B.jj + 8.0) - 1.0) {
    h = 0;
    ny = 0;
  } else {
    h = static_cast<int32_T>(scooby_jetsonV2_B.jj) - 1;
    ny = static_cast<int32_T>((scooby_jetsonV2_B.jj + 8.0) - 1.0);
  }

  ny -= h;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data[i] = arr_data[h + i];
  }

  if (ny == 0) {
    ny = 0;
  } else {
    ny >>= 3;
  }

  memcpy((void *)&scooby_jetsonV2_B.leaf_len_data[0], (void *)
         &scooby_jetsonV2_B.x_data[0], (uint32_T)((size_t)ny * sizeof(real_T)));
  scooby_jetsonV2_B.i = ((scooby_jetsonV2_B.jj + 8.0) +
    scooby_jetsonV2_B.leaf_len_data[0]) - 1.0;
  if (scooby_jetsonV2_B.jj + 8.0 > scooby_jetsonV2_B.i) {
    h = 0;
    j = 0;
  } else {
    h = static_cast<int32_T>(scooby_jetsonV2_B.jj + 8.0) - 1;
    j = static_cast<int32_T>(scooby_jetsonV2_B.i);
  }

  ny = j - h;
  for (i = 0; i < ny; i++) {
    scooby_jetsonV2_B.x_data[i] = arr_data[h + i];
  }

  memcpy((void *)&scooby_jetsonV2_B.temp_data_g1[0], (void *)
         &scooby_jetsonV2_B.x_data[0], (uint32_T)((size_t)(j - h) * sizeof
          (uint8_T)));
  scooby_jetsonV2_inject_data_j4(arr_data, (scooby_jetsonV2_B.jj + 8.0) +
    scooby_jetsonV2_B.leaf_len_data[0],
    &msg_out_child_frame_id_SL_Info->CurrentLength,
    &msg_out_child_frame_id_SL_Info->ReceivedLength, &scooby_jetsonV2_B.jj);
  scooby_jetsonV2_inject_data_j4t(arr_data, scooby_jetsonV2_B.jj,
    &msg_out_pose->pose.position.x, &msg_out_pose->pose.position.y,
    &msg_out_pose->pose.position.z, &msg_out_pose->pose.orientation,
    msg_out_pose->covariance, &scooby_jetsonV2_B.jj);
  scooby_jetso_inject_data_j4t4lg(arr_data, scooby_jetsonV2_B.jj,
    &msg_out_twist->twist.linear.x, &msg_out_twist->twist.linear.y,
    &msg_out_twist->twist.linear.z, &msg_out_twist->twist.angular.x,
    &msg_out_twist->twist.angular.y, &msg_out_twist->twist.angular.z,
    msg_out_twist->covariance, &scooby_jetsonV2_B.jj);
  memcpy(&msg_out_child_frame_id[0], &scooby_jetsonV2_B.temp_data_g1[0], sizeof
         (uint8_T) << 7U);
}

void scooby_jetsonV2ModelClass::scooby_je_rosmsgSerializeStruct(real_T msg_x,
  real_T msg_y, real_T msg_z, uint8_T y[2048])
{
  real_T x;
  int32_T i;
  uint8_T new_vec[8];
  uint8_T new_vec_len[8];
  memset(&y[0], 0, sizeof(uint8_T) << 11U);

  // rosmsgSerializeStruct Serialize a message structure into a byte stream
  //    Copyright 2017 The MathWorks, Inc.
  memcpy((void *)&new_vec[0], (void *)&msg_x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    y[i] = new_vec_len[i];
    y[i + 8] = new_vec[i];
  }

  memcpy((void *)&new_vec[0], (void *)&msg_y, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    y[i + 16] = new_vec_len[i];
    y[i + 24] = new_vec[i];
  }

  memcpy((void *)&new_vec[0], (void *)&msg_z, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    y[i + 32] = new_vec_len[i];
    y[i + 40] = new_vec[i];
  }
}

void scooby_jetsonV2ModelClass::scooby_j_ros2serialize_stepImpl(real_T
  Msg_linear_x, real_T Msg_linear_y, real_T Msg_linear_z, real_T Msg_angular_x,
  real_T Msg_angular_y, real_T Msg_angular_z, uint8_T Data[2048])
{
  real_T x;
  int32_T i;
  uint8_T new_vec_len[8];

  // rosmsgSerializeStruct Serialize a message structure into a byte stream
  //    Copyright 2017 The MathWorks, Inc.
  scooby_je_rosmsgSerializeStruct(Msg_linear_x, Msg_linear_y, Msg_linear_z, Data);

  // rosmsgSerializeStruct Serialize a message structure into a byte stream
  //    Copyright 2017 The MathWorks, Inc.
  memcpy((void *)&scooby_jetsonV2_B.new_vec[0], (void *)&Msg_angular_x,
         (uint32_T)((size_t)8 * sizeof(uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    Data[i + 48] = new_vec_len[i];
    Data[i + 56] = scooby_jetsonV2_B.new_vec[i];
  }

  memcpy((void *)&scooby_jetsonV2_B.new_vec[0], (void *)&Msg_angular_y,
         (uint32_T)((size_t)8 * sizeof(uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    Data[i + 64] = new_vec_len[i];
    Data[i + 72] = scooby_jetsonV2_B.new_vec[i];
  }

  memcpy((void *)&scooby_jetsonV2_B.new_vec[0], (void *)&Msg_angular_z,
         (uint32_T)((size_t)8 * sizeof(uint8_T)));
  x = 8.0;
  memcpy((void *)&new_vec_len[0], (void *)&x, (uint32_T)((size_t)8 * sizeof
          (uint8_T)));

  // packed = horzcat(packed, new_vec_len);
  for (i = 0; i < 8; i++) {
    Data[i + 80] = new_vec_len[i];
    Data[i + 88] = scooby_jetsonV2_B.new_vec[i];
  }
}

void scooby_jetsonV2ModelClass::scooby_jetsonV_SystemCore_setup
  (ros_slros2_internal_block_Pub_T *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  char_T b_zeroDelimTopic[9];
  static const char_T tmp[8] = { '/', 'o', 'd', 'o', 'm', '_', 's', 'p' };

  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 8; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[8] = '\x00';
  ros2::matlab::create_Pub_scooby_jetsonV2_125(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

void scooby_jetsonV2ModelClass::scooby_jetso_SystemCore_setup_j
  (ros_slros2_internal_block_Sub_T *obj)
{
  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  char_T b_zeroDelimTopic[9];
  static const char_T tmp[8] = { '/', 'c', 'm', 'd', '_', 'v', 'e', 'l' };

  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 8; i++) {
    b_zeroDelimTopic[i] = tmp[i];
  }

  b_zeroDelimTopic[8] = '\x00';
  ros2::matlab::create_Sub_scooby_jetsonV2_126(&b_zeroDelimTopic[0], qos_profile);
  obj->isSetupComplete = true;
}

// Model step function
void scooby_jetsonV2ModelClass::step()
{
  uint16_T UDPReceive2_o2;
  char_T *sErr;
  boolean_T b_varargout_1;

  // S-Function (sdspFromNetwork): '<Root>/UDP Receive2'
  sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
  scooby_jetsonV2_B.samplesRead = 2048;
  LibOutputs_Network(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U],
                     &scooby_jetsonV2_B.UDPReceive2_o1[0U],
                     &scooby_jetsonV2_B.samplesRead);
  if (*sErr != 0) {
    rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
    rtmSetStopRequested((&scooby_jetsonV2_M), 1);
  }

  // S-Function (sdspFromNetwork): '<Root>/UDP Receive2'
  UDPReceive2_o2 = static_cast<uint16_T>(scooby_jetsonV2_B.samplesRead);

  // Outputs for Enabled SubSystem: '<Root>/Deserialize1' incorporates:
  //   EnablePort: '<S3>/Enable'

  // RelationalOperator: '<S2>/Compare' incorporates:
  //   Constant: '<S2>/Constant'

  if (UDPReceive2_o2 > scooby_jetsonV2_P.Constant_Value_f) {
    // MATLABSystem: '<S3>/ros2deserialize' incorporates:
    //   S-Function (sdspFromNetwork): '<Root>/UDP Receive2'

    for (scooby_jetsonV2_B.samplesRead = 0; scooby_jetsonV2_B.samplesRead < 2048;
         scooby_jetsonV2_B.samplesRead++) {
      scooby_jetsonV2_B.u0[scooby_jetsonV2_B.samplesRead] =
        scooby_jetsonV2_B.UDPReceive2_o1[scooby_jetsonV2_B.samplesRead];
    }

    scooby_jetsonV2_B.loop_ub = UDPReceive2_o2;
    for (scooby_jetsonV2_B.samplesRead = 0; scooby_jetsonV2_B.samplesRead <
         scooby_jetsonV2_B.loop_ub; scooby_jetsonV2_B.samplesRead++) {
      scooby_jetsonV2_B.u0_data[scooby_jetsonV2_B.samplesRead] =
        scooby_jetsonV2_B.u0[scooby_jetsonV2_B.samplesRead];
    }

    scooby_jetsonV2_rosmsgUnpack(scooby_jetsonV2_B.u0_data,
      &scooby_jetsonV2_B.ros2deserialize.header.stamp,
      scooby_jetsonV2_B.ros2deserialize.header.frame_id,
      &scooby_jetsonV2_B.ros2deserialize.header.frame_id_SL_Info,
      scooby_jetsonV2_B.ros2deserialize.child_frame_id,
      &scooby_jetsonV2_B.ros2deserialize.child_frame_id_SL_Info,
      &scooby_jetsonV2_B.ros2deserialize.pose,
      &scooby_jetsonV2_B.ros2deserialize.twist);

    // End of MATLABSystem: '<S3>/ros2deserialize'
  }

  // End of RelationalOperator: '<S2>/Compare'
  // End of Outputs for SubSystem: '<Root>/Deserialize1'

  // MATLABSystem: '<S4>/SinkBlock'
  ros2::matlab::publish_Pub_scooby_jetsonV2_125
    (&scooby_jetsonV2_B.ros2deserialize);

  // MATLABSystem: '<S6>/SourceBlock'
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_scooby_jetsonV2_126
    (&scooby_jetsonV2_B.b_varargout_2);

  // Outputs for Enabled SubSystem: '<Root>/Send Message over UDP' incorporates:
  //   EnablePort: '<S5>/Enable'

  // Outputs for Enabled SubSystem: '<S6>/Enabled Subsystem' incorporates:
  //   EnablePort: '<S7>/Enable'

  if (b_varargout_1) {
    // MATLABSystem: '<S5>/ros2serialize2'
    scooby_j_ros2serialize_stepImpl(scooby_jetsonV2_B.b_varargout_2.linear.x,
      scooby_jetsonV2_B.b_varargout_2.linear.y,
      scooby_jetsonV2_B.b_varargout_2.linear.z,
      scooby_jetsonV2_B.b_varargout_2.angular.x,
      scooby_jetsonV2_B.b_varargout_2.angular.y,
      scooby_jetsonV2_B.b_varargout_2.angular.z,
      scooby_jetsonV2_B.ros2serialize2);

    // Update for S-Function (sdspToNetwork): '<S5>/UDP Send' incorporates:
    //   MATLABSystem: '<S5>/ros2serialize2'

    sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
    LibUpdate_Network(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U],
                      &scooby_jetsonV2_B.ros2serialize2[0U], 2048);
    if (*sErr != 0) {
      rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
      rtmSetStopRequested((&scooby_jetsonV2_M), 1);
    }

    // End of Update for S-Function (sdspToNetwork): '<S5>/UDP Send'
  }

  // End of MATLABSystem: '<S6>/SourceBlock'
  // End of Outputs for SubSystem: '<S6>/Enabled Subsystem'
  // End of Outputs for SubSystem: '<Root>/Send Message over UDP'
}

// Model initialize function
void scooby_jetsonV2ModelClass::initialize()
{
  {
    char_T *sErr;

    // Start for S-Function (sdspFromNetwork): '<Root>/UDP Receive2'
    sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
    CreateUDPInterface(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U], 0,
                        "0.0.0.0", scooby_jetsonV2_P.UDPReceive2_localPort,
                        "0.0.0.0", -1, 8192, 1, MIN_int32_T);
    }

    if (*sErr == 0) {
      LibStart(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
        rtmSetStopRequested((&scooby_jetsonV2_M), 1);
      }
    }

    // End of Start for S-Function (sdspFromNetwork): '<Root>/UDP Receive2'

    // SystemInitialize for Enabled SubSystem: '<Root>/Deserialize1'
    // SystemInitialize for MATLABSystem: '<S3>/ros2deserialize' incorporates:
    //   Outport: '<S3>/Msg'

    scooby_jetsonV2_B.ros2deserialize = scooby_jetsonV2_P.Msg_Y0;

    // End of SystemInitialize for SubSystem: '<Root>/Deserialize1'

    // SystemInitialize for Enabled SubSystem: '<Root>/Send Message over UDP'
    // Start for S-Function (sdspToNetwork): '<S5>/UDP Send'
    sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
    CreateUDPInterface(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U], 1, "0.0.0.0",
                        -1, "192.168.1.20", scooby_jetsonV2_P.UDPSend_remotePort,
                        2048, 1, 0);
    }

    if (*sErr == 0) {
      LibStart(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
        rtmSetStopRequested((&scooby_jetsonV2_M), 1);
      }
    }

    // End of Start for S-Function (sdspToNetwork): '<S5>/UDP Send'

    // Start for MATLABSystem: '<S5>/ros2serialize2'
    scooby_jetsonV2_DW.obj_b.isInitialized = 0;
    scooby_jetsonV2_DW.obj_b.isInitialized = 1;

    // End of SystemInitialize for SubSystem: '<Root>/Send Message over UDP'

    // Start for MATLABSystem: '<S4>/SinkBlock'
    scooby_jetsonV2_DW.obj.matlabCodegenIsDeleted = true;
    scooby_jetsonV2_DW.obj.isInitialized = 0;
    scooby_jetsonV2_DW.obj.matlabCodegenIsDeleted = false;
    scooby_jetsonV_SystemCore_setup(&scooby_jetsonV2_DW.obj);

    // Start for MATLABSystem: '<S6>/SourceBlock'
    scooby_jetsonV2_DW.obj_n.matlabCodegenIsDeleted = true;
    scooby_jetsonV2_DW.obj_n.isInitialized = 0;
    scooby_jetsonV2_DW.obj_n.matlabCodegenIsDeleted = false;
    scooby_jetso_SystemCore_setup_j(&scooby_jetsonV2_DW.obj_n);
  }
}

// Model terminate function
void scooby_jetsonV2ModelClass::terminate()
{
  char_T *sErr;

  // Terminate for S-Function (sdspFromNetwork): '<Root>/UDP Receive2'
  sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
  LibTerminate(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
    rtmSetStopRequested((&scooby_jetsonV2_M), 1);
  }

  LibDestroy(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U], 0);
  DestroyUDPInterface(&scooby_jetsonV2_DW.UDPReceive2_NetworkLib[0U]);

  // End of Terminate for S-Function (sdspFromNetwork): '<Root>/UDP Receive2'

  // Terminate for MATLABSystem: '<S4>/SinkBlock'
  if (!scooby_jetsonV2_DW.obj.matlabCodegenIsDeleted) {
    scooby_jetsonV2_DW.obj.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S4>/SinkBlock'

  // Terminate for MATLABSystem: '<S6>/SourceBlock'
  if (!scooby_jetsonV2_DW.obj_n.matlabCodegenIsDeleted) {
    scooby_jetsonV2_DW.obj_n.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S6>/SourceBlock'

  // Terminate for Enabled SubSystem: '<Root>/Send Message over UDP'
  // Terminate for S-Function (sdspToNetwork): '<S5>/UDP Send'
  sErr = GetErrorBuffer(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
  LibTerminate(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus((&scooby_jetsonV2_M), sErr);
    rtmSetStopRequested((&scooby_jetsonV2_M), 1);
  }

  LibDestroy(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U], 1);
  DestroyUDPInterface(&scooby_jetsonV2_DW.UDPSend_NetworkLib[0U]);

  // End of Terminate for S-Function (sdspToNetwork): '<S5>/UDP Send'
  // End of Terminate for SubSystem: '<Root>/Send Message over UDP'
}

// Constructor
scooby_jetsonV2ModelClass::scooby_jetsonV2ModelClass() :
  scooby_jetsonV2_B(),
  scooby_jetsonV2_DW(),
  scooby_jetsonV2_M()
{
  // Currently there is no constructor body generated.
}

// Destructor
scooby_jetsonV2ModelClass::~scooby_jetsonV2ModelClass()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
RT_MODEL_scooby_jetsonV2_T * scooby_jetsonV2ModelClass::getRTM()
{
  return (&scooby_jetsonV2_M);
}

//
// File trailer for generated code.
//
// [EOF]
//
