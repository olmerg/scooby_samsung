# Scooby Localization Package

## Instalation Procedures

* Lembrar-se de instalar o pacote pyserial, necessêrio para o sistema IIDRE

## MARVELMIND:
1. Posicionar os beaccons de referóncia e definir origem do sistema de coordenadas
2. Configurar posições dos beacons de referênncia pelo dashboard;
3. Verificar endereço da porta serial na qual as informaçêes estõo sendo enviadas
4. Abrir o terminal (115200 bps) serial para confirmar o recebimento dos dados
5. Fechar o terminal serial para evitar conflitos
6. Para executar o nós ROS, na raiz do workspace digitar:

    ./build/marvelmind_nav/hedge_rcv_bin /dev/ttyACM{}

## IIDRE:
1. Posicionar os beaccons de referóncia e definir origem do sistema de coordenadas
2. Configurar posições dos beacons de referõncia via GUI ou comando AT
3. Verificar endereço da porta serial na qual as informaçêes estõo sendo enviadas
4. Abrir o terminal (115200 bps) serial para confirmar o recebimento dos dados
5. Fechar o terminal serial para evitar conflitos
6. Para executar o nós ROS, na raiz do workspace digitar:

     ros2 launch iidre.launch.py