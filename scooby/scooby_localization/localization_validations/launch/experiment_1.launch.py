# Copyright 2020 Giovani Bernardes
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch the iidre localization package."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    #scooby_localization_pkg = get_package_share_directory('scooby_localization')
    scooby_description_pkg=get_package_share_directory('scooby_description')
    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')


    use_rviz = LaunchConfiguration('use_rviz')
    urdf_file= LaunchConfiguration('urdf_file')

    static_transform_iidre = Node(package = "tf2_ros", 
                       executable = "static_transform_publisher",
                       arguments = ["0.0", "0.0", "0", "0", "0", "0", "map", "odom"])

    static_transform_slam = Node(package = "tf2_ros", 
                       executable = "static_transform_publisher",
                       arguments = ["-0.765", "1.015", "0.0", "0.0", "0", "0", "odom_slam", "odom"])

    static_transform_odom = Node(package = "tf2_ros", 
                       executable = "static_transform_publisher",
                       arguments = ["-0.765", "1.015", "0.0", "0.0", "0", "0", "map", "odom"])

    #static_transform_uwb = Node(package = "tf2_ros", 
    #                   executable = "static_transform_publisher",
    #                   arguments = ["-10", "0", "0", "0", "0", "0", "base_link", "uwb"])

    #static_transform_hedge = Node(package = "tf2_ros", 
    #                   executable = "static_transform_publisher",
    #                   arguments = ["-10", "0", "0", "0", "0", "0", "hedge", "base_link"])

    utils = Node(
        package='localization_validations',
        parameters=[{'use_sim_time': True}],
        executable='localization_eval')

    localization_node = Node(
        package='localization_validations',
        parameters=[{'use_sim_time': True}],
        executable='localization_node')

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', os.path.join(get_package_share_directory('localization_validations'), 'rviz', 'rviz_config.rviz'), {'use_sim_time': True}],
    )

    ekf_cont = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("localization_filters"), 'param', 'continuous_ekf.yaml'), {'use_sim_time': False}],
            remappings = [('odometry/filtered', 'kf_odom')]
           )

    ekf_discr = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("localization_filters"), 'param', 'global_ekf.yaml'), {'use_sim_time': False}],
            remappings = [('odometry/filtered', 'kf_global')]
           )

    amcl = Node(
            package='nav2_amcl',
            executable='amcl',
            name='amcl',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("localization_filters"), 'param', 'amcl.yaml'), {'use_sim_time': False}],
            remappings = [('odom', 'amcl_odom')]
           )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'use_sim_time': True}],
        #remappings=[
        #    ("joint_states", "scooby/joint_states")
        #],
        arguments=[urdf_file])

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gzserver.launch.py'),
        ),
        condition=(IfCondition(LaunchConfiguration('verbose')))
        #condition=IfCondition(LaunchConfiguration('init')),
        #condition=IfCondition(LaunchConfiguration('factory')),
        #condition=IfCondition(LaunchConfiguration('force_system'))
    )

    return LaunchDescription([

        DeclareLaunchArgument('use_rviz', default_value='true',
                              description='Open RViz.'),
        DeclareLaunchArgument('urdf_file',default_value=os.path.join(scooby_description_pkg, 'urdf', 'Snoopy.urdf'),
                              description='urddeclare_urdf_cmd =f file complete path'),
        DeclareLaunchArgument('verbose', default_value='true',
                              description='Set verbose to "true" to run gzserver.'),

        #gazebo,
        static_transform_iidre,
        #static_transform_slam,
        #static_transform_odom,
        #static_transform_uwb,
        #static_transform_hedge,
        robot_state_publisher,
        #amcl,
        utils,
        rviz,
        ekf_cont,
        ekf_discr,
        localization_node
    ])
