#include "rclcpp/rclcpp.hpp"
#include "moving_average.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/multi_array_dimension.hpp"
#include "std_msgs/msg/int32_multi_array.hpp"
#include "std_msgs/msg/float64.hpp"
#include "localization_interfaces/msg/hedge_quality.hpp"
#include <nav_msgs/msg/odometry.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/buffer_interface.h>
#include <tf2_ros/visibility_control.h>
#include <tf2/buffer_core.h>
#include <tf2/time.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/msg/transform_stamped.hpp>

#include <chrono>

using namespace std::chrono_literals;

#define ROS_NODE_NAME "localization_node"
#define PI 3.1415

class LocalizationNode : public rclcpp::Node
{
  public:
    LocalizationNode()
    : Node(ROS_NODE_NAME)
    {
      auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());

      // ************** Declare publishers for rviz visualization **************
      //pubMarvelQuality = this->create_publisher<std_msgs::msg::Float64>("marvelmind_quality", 1);
			//pubIidreQuality = this->create_publisher<std_msgs::msg::Int32MultiArray>("iidre_quality", 1);
      pubOdom = this->create_publisher<nav_msgs::msg::Odometry>("odom_with_covariance", 1);
      pubTwist = this->create_publisher<geometry_msgs::msg::TwistWithCovariance>("Twist_with_covariance", 1);
      pubIidreOdom = this->create_publisher<nav_msgs::msg::Odometry>("iidre_with_covariance", 1);
      pubMarvel1Odom = this->create_publisher<nav_msgs::msg::Odometry>("marvel1_with_covariance", 1);
      pubMarvel2Odom = this->create_publisher<nav_msgs::msg::Odometry>("marvel2_with_covariance", 1);
      pubImu = this->create_publisher<sensor_msgs::msg::Imu>("imu_with_covariance", 1);
      pubTf = this->create_publisher<tf2_msgs::msg::TFMessage>("tf", 1);
      //pubQualBeacon1 = this->create_publisher<std_msgs::msg::Float64>("iidre_qual_1", 1);
      //pubQualBeacon2 = this->create_publisher<std_msgs::msg::Float64>("iidre_qual_2", 1);
      //pubQualBeacon3 = this->create_publisher<std_msgs::msg::Float64>("iidre_qual_3", 1);
      //pubQualBeacon4 = this->create_publisher<std_msgs::msg::Float64>("iidre_qual_4", 1);
      pubIidreHeading = this->create_publisher<std_msgs::msg::Float64>("iidre_heading", 1);
      pubImuHeading = this->create_publisher<std_msgs::msg::Float64>("imu_heading", 1);

      // ************** Declare subscribers **************
      subMarvelmind1Quality = this->create_subscription
        <localization_interfaces::msg::HedgeQuality>(
        "marvel1/hedge_quality", 10, std::bind(&LocalizationNode::marvelmind1QualityCallback, this, std::placeholders::_1)); 

      subMarvelmind2Quality = this->create_subscription
        <localization_interfaces::msg::HedgeQuality>(
        "marvel2/hedge_quality", 10, std::bind(&LocalizationNode::marvelmind2QualityCallback, this, std::placeholders::_1)); 

      subOdom = this->create_subscription
        <nav_msgs::msg::Odometry>(
        "/odom", default_qos, std::bind(&LocalizationNode::odomCallback, this, std::placeholders::_1));

      subIidreOdom = this->create_subscription
        <nav_msgs::msg::Odometry>(
        "/iidre_pose", 10, std::bind(&LocalizationNode::odomIidreCallback, this, std::placeholders::_1));

      subMarvel1Odom = this->create_subscription
        <nav_msgs::msg::Odometry>(
        "marvel1/hedge_pos", 10, std::bind(&LocalizationNode::odomMarvel1Callback, this, std::placeholders::_1));

      subMarvel2Odom = this->create_subscription
        <nav_msgs::msg::Odometry>(
        "marvel2/hedge_pos", 10, std::bind(&LocalizationNode::odomMarvel2Callback, this, std::placeholders::_1));

      subImu = this->create_subscription
      <sensor_msgs::msg::Imu>(
      "imu", 10, std::bind(&LocalizationNode::imuCallback, this, std::placeholders::_1));

      subTf = this->create_subscription
      <tf2_msgs::msg::TFMessage>(
      "tf_old", 10, std::bind(&LocalizationNode::tfCallback, this, std::placeholders::_1));

      subStaticTf = this->create_subscription
      <tf2_msgs::msg::TFMessage>(
      "tf_static", 10, std::bind(&LocalizationNode::staticTfCallback, this, std::placeholders::_1));

      // ************** Set default values for sensors and covariances **************
      setDefault();
      
    }

  private:
    //  ************** DATA TYPES **************
    
    struct axis
    {
      float x = 0;
      float y = 0;
      float z = 0;
    };

    struct averagedAxis
    {
      MovingAverage *x;
      MovingAverage *y;
      MovingAverage *z;
    };
    
    struct imuError
    {
      averagedAxis accelerometer;
      averagedAxis gyro; 
    };

    struct beacon
    {
      geometry_msgs::msg::Point bodyPose;
    };

    //  ************** PUBLISHERS AND SUBSCRIBERS **************
    // --- Publishers
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubMarvelQuality;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubIidreHeading;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubImuHeading;
		
		rclcpp::Publisher<std_msgs::msg::Int32MultiArray>::SharedPtr pubIidreQuality;

    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pubOdom;

    rclcpp::Publisher<geometry_msgs::msg::TwistWithCovariance>::SharedPtr pubTwist;

    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pubIidreOdom;

    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pubMarvel1Odom;

    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pubMarvel2Odom;

    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr pubImu;

    rclcpp::Publisher<tf2_msgs::msg::TFMessage>::SharedPtr pubTf;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubQualBeacon1;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubQualBeacon2;
    
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubQualBeacon3;
    
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubQualBeacon4;

    // --- Subscribers
    rclcpp::Subscription<localization_interfaces::msg::HedgeQuality>::SharedPtr subMarvelmind1Quality;

    rclcpp::Subscription<localization_interfaces::msg::HedgeQuality>::SharedPtr subMarvelmind2Quality;

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subOdom;

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subIidreOdom;

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subMarvel1Odom;

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subMarvel2Odom;

    rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr subImu;

    rclcpp::Subscription<tf2_msgs::msg::TFMessage>::SharedPtr subTf;

    rclcpp::Subscription<tf2_msgs::msg::TFMessage>::SharedPtr subStaticTf;


  //  ************** AUX VARIABLES **************
    imuError imuOffset;
    float odomOffset[3] = {-0.7075, 1.0525, 3.1415};

    int imuCount;

    //float imuFrequency = 100; // [Hz]

    bool is_moving = false;

    builtin_interfaces::msg::Time imuLastMeas;

    nav_msgs::msg::Odometry calcOdom;
    nav_msgs::msg::Odometry calcMarvel;
    nav_msgs::msg::Odometry calcIdre;

    sensor_msgs::msg::Imu calcImu;

    beacon marvelmindBeacon1;
    beacon marvelmindBeacon2;
    beacon iidreBeacon1;
    
    float ROBOT_X_SIZE = 1.415;
    float ROBOT_Y_SIZE = 0.855; 

    float headingImu = 0.0;
    float headingOdom = 0.0;
    float headingIidre = 0.0;
    float headingMarvel1 = 0.0;
    float headingMarvel2 = 0.0;

    float marvel1Quality = 0.0;
    float marvel2Quality = 0.0;

    axis iidreLastPosition;
    axis marvel1LastPosition;
    axis marvel2LastPosition;

    averagedAxis iidreBuffer;
    averagedAxis marvel1Buffer;
    averagedAxis marvel2Buffer;

  //  ************** AUX METHODS **************

  void setDefault()
  {
    //  ************** Set sensors default covariances **************
      // --- IMU
      calcImu.angular_velocity_covariance[0] = 0.3;
      calcImu.angular_velocity_covariance[4] = 0.3;
      calcImu.angular_velocity_covariance[8] = 0.1;

      calcImu.linear_acceleration_covariance[0] = 1.7;
      calcImu.linear_acceleration_covariance[4] = 1.7;
      calcImu.linear_acceleration_covariance[8] = 1.7;

      calcImu.orientation_covariance[0] = 0.5;
      calcImu.orientation_covariance[4] = 0.5;
      calcImu.orientation_covariance[8] = 0.5;

      // --- Odometry
      calcOdom.pose.covariance[0] = 2.0;
      calcOdom.pose.covariance[7] = 2.0;
      calcOdom.pose.covariance[14] = 1;
      calcOdom.pose.covariance[21] = 0.1;
      calcOdom.pose.covariance[28] = 0.1;
      calcOdom.pose.covariance[35] = 0.05;

      calcOdom.twist.covariance[0] = 0.0001;
      calcOdom.twist.covariance[7] = 0.0001;
      calcOdom.twist.covariance[14] = 0.0001;
      calcOdom.twist.covariance[21] = 0.0001;
      calcOdom.twist.covariance[28] = 0.0001;
      calcOdom.twist.covariance[35] = 0.001;

      // --- IIDRE
      calcIdre.pose.covariance[0] = 0.9;
      calcIdre.pose.covariance[7] = 0.9;
      calcIdre.pose.covariance[14] = 1;
      calcIdre.pose.covariance[21] = 0.3;
      calcIdre.pose.covariance[28] = 0.3;
      calcIdre.pose.covariance[35] = 0.3;

      calcIdre.twist.covariance[0] = 1000;
      calcIdre.twist.covariance[7] = 1000;
      calcIdre.twist.covariance[14] = 1;
      calcIdre.twist.covariance[21] = 1;
      calcIdre.twist.covariance[28] = 1;
      calcIdre.twist.covariance[35] = 100;

      // --- Marvel
      calcMarvel.pose.covariance[0] = 0.5;
      calcMarvel.pose.covariance[7] = 0.5;
      calcMarvel.pose.covariance[14] = 1;
      calcMarvel.pose.covariance[21] = 0.3;
      calcMarvel.pose.covariance[28] = 0.3;
      calcMarvel.pose.covariance[35] = 0.3;

      calcMarvel.twist.covariance[0] = 1000;
      calcMarvel.twist.covariance[7] = 1000;
      calcMarvel.twist.covariance[14] = 1;
      calcMarvel.twist.covariance[21] = 1;
      calcMarvel.twist.covariance[28] = 1;
      calcMarvel.twist.covariance[35] = 100;

      imuCount = 0;

      // Set the size for the imu offset average array
      imuOffset.accelerometer.x = new MovingAverage(5);
      imuOffset.accelerometer.y = new MovingAverage(5);
      imuOffset.accelerometer.z = new MovingAverage(5);

      imuOffset.gyro.x = new MovingAverage(5);
      imuOffset.gyro.y = new MovingAverage(5);
      imuOffset.gyro.z = new MovingAverage(5);

      iidreBuffer.x = new MovingAverage(20);
      iidreBuffer.y = new MovingAverage(20);
      iidreBuffer.z = new MovingAverage(20);

      marvel1Buffer.x = new MovingAverage(1);
      marvel1Buffer.y = new MovingAverage(1);
      marvel1Buffer.z = new MovingAverage(1);

      marvel2Buffer.x = new MovingAverage(1);
      marvel2Buffer.y = new MovingAverage(1);
      marvel2Buffer.z = new MovingAverage(1);

      iidreLastPosition.x = 0.0;
      iidreLastPosition.y = 0.0;

      marvel1LastPosition.x = 0.0;
      marvel1LastPosition.y = 0.0;

      marvel2LastPosition.x = 0.0;
      marvel2LastPosition.y = 0.0;
  }

  float orientationFromMarvel()
  {
    float slope_between_sensors = (marvel1Buffer.y->mean() - marvel2Buffer.y->mean())/(marvel1Buffer.x->mean() - marvel2Buffer.x->mean());
    float slope_offset = (marvelmindBeacon1.bodyPose.y  - marvelmindBeacon2.bodyPose.y )/(marvelmindBeacon1.bodyPose.x  - marvelmindBeacon2.bodyPose.x);
    float position_offset = 0.0;

    if(marvel2Buffer.y->mean() > marvel1Buffer.y->mean())
      position_offset -= PI;

    return normalizeAngle(atan(slope_between_sensors - slope_offset) + position_offset);
  }
  
  float normalizeAngle(float angle)
  {
    if(angle > PI)
      angle -= 2*PI;
    else if(angle < -PI)
      angle += 2*PI;

    return angle;
  }
  
  bool distanceValidity(int sensor)
  {
    float marvel_1_to_2 = sqrt((marvel1Buffer.x->mean() - marvel2Buffer.x->mean()) * (marvel1Buffer.x->mean() - marvel2Buffer.x->mean()) 
                          + (marvel1Buffer.y->mean() - marvel2Buffer.y->mean()) * (marvel1Buffer.y->mean() - marvel2Buffer.y->mean()));
    
    float marvel_1_to_iidre = sqrt((marvel1Buffer.x->mean() - iidreBuffer.x->mean()) * (marvel1Buffer.x->mean() - iidreBuffer.x->mean()) 
                              + (marvel1Buffer.y->mean() - iidreBuffer.y->mean()) * (marvel1Buffer.y->mean() - iidreBuffer.y->mean()));

    float marvel_2_to_iidre = sqrt((marvel2Buffer.x->mean() - iidreBuffer.x->mean()) * (marvel2Buffer.x->mean() - iidreBuffer.x->mean()) 
                              + (marvel2Buffer.y->mean() - iidreBuffer.y->mean()) * (marvel2Buffer.y->mean() - iidreBuffer.y->mean()));

    // if marvel 1
    if((sensor == 1) && ((fabs(marvel_1_to_2) > 2.0) || (fabs(marvel_1_to_iidre) > 2.0)))
      return false;

    // if marvel 2
    if((sensor == 2) && ((fabs(marvel_1_to_2) > 2.0) || (fabs(marvel_2_to_iidre) > 2.0)))
      return false;

    // if iidre
    if((sensor == 3) && ((fabs(marvel_1_to_iidre) > 2.0) || (fabs(marvel_1_to_iidre) > 2.0)))
      return false;

    return true;
  }

  //  ************** CALLBACKS **************
  void marvelmind1QualityCallback(const localization_interfaces::msg::HedgeQuality::SharedPtr hedge_quality_msg)
  {
    //RCLCPP_INFO(this->get_logger(), "Publishing quality data");	
    marvel1Quality = hedge_quality_msg->quality_percents;
  }

  void marvelmind2QualityCallback(const localization_interfaces::msg::HedgeQuality::SharedPtr hedge_quality_msg)
  {
    //RCLCPP_INFO(this->get_logger(), "Publishing quality data");	
    marvel2Quality = hedge_quality_msg->quality_percents;
  }

  void odomCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    //RCLCPP_INFO(this->get_logger(), "Publishing odom with covariance");	
    // ------- Save orientation from Odom measurement
    tf2::Quaternion q(odom->pose.pose.orientation.x,
                      odom->pose.pose.orientation.y,
                      odom->pose.pose.orientation.z,
                      odom->pose.pose.orientation.w);

    tf2::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    //this->headingOdom = - yaw - PI;
    headingOdom = yaw;

    if(headingOdom > PI)
      headingOdom -= 2*PI;
    else if(headingOdom < -PI)
      headingOdom += 2*PI;

    //q.setRPY(roll, pitch, this->headingOdom);

    //odom->pose.pose.orientation.x = q.x();
    //odom->pose.pose.orientation.y = q.y();
    //odom->pose.pose.orientation.z = q.z();
    //odom->pose.pose.orientation.w = q.w();

    // Republish raw odom values with calculated odometry covariances 
    odom->pose.covariance = calcOdom.pose.covariance;
    
    //odom->pose.pose.position.x = odomOffset[0] - odom->pose.pose.position.x;
    //odom->pose.pose.position.y = odomOffset[1] + odom->pose.pose.position.y;
    //odom->pose.pose.position.x = odom->pose.pose.position.x;
    //odom->pose.pose.position.y = odom->pose.pose.position.y;

    // Verify foward velocity in order to determine whether the robot is moving or not using a set threshold
    if(std::fabs(odom->twist.twist.linear.x) < 0.01 )
    {
      // Set movement flag
      is_moving = false;

      // If the robot is not moving, increase IMU covariance
      calcImu.linear_acceleration_covariance[0] = 0.5;
      calcImu.linear_acceleration_covariance[4] = 0.5;
      calcImu.linear_acceleration_covariance[8] = 0.5;  
    } else
    {
      // Set movement flag
      is_moving = true;

      // If the robot is moving, decrease IMU covariance
      calcImu.linear_acceleration_covariance[0] = 0.1;
      calcImu.linear_acceleration_covariance[4] = 0.1;
      calcImu.linear_acceleration_covariance[8] = 0.1;  
    }

    geometry_msgs::msg::TwistWithCovariance twistMsg = odom->twist;
    
    this->pubTwist->publish(twistMsg);
    this->pubOdom->publish(*odom);
  }

  void odomIidreCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    //RCLCPP_INFO(this->get_logger(), "Publishing odom with covariance");	

    odom->child_frame_id = "uwb";
    odom->header.frame_id = "map";

    // Odometry covariances as the calculated values
    odom->pose.covariance = calcIdre.pose.covariance;

    // Save current values to remove offsets
    iidreBeacon1.bodyPose.x = -0.6586;
    iidreBeacon1.bodyPose.y = 0.597;

    // Calculate offsets 
    float OFFSET_X = iidreBeacon1.bodyPose.x*cos(headingOdom) - iidreBeacon1.bodyPose.y*sin(headingOdom);
    float OFFSET_Y = iidreBeacon1.bodyPose.y*cos(headingOdom) + iidreBeacon1.bodyPose.x*sin(headingOdom);

    iidreBuffer.x->push(odom->pose.pose.position.x - OFFSET_X);
    iidreBuffer.y->push(odom->pose.pose.position.y - OFFSET_Y);

    // Remove position offset due to beacon placement in the robot
    odom->pose.pose.position.x = iidreBuffer.x->mean(); 
    odom->pose.pose.position.y = iidreBuffer.y->mean(); 
    odom->pose.pose.position.z = 0.0; 

    // Estimate orientation from trajectory
    headingIidre = orientationFromMarvel();

    // Save orientation data in odom message
    tf2::Quaternion q;
    q.setRPY(0, 0, headingIidre);  // Create this quaternion from roll/pitch/yaw (in radians)
    odom->pose.pose.orientation.x = q.x();
    odom->pose.pose.orientation.y = q.y();
    odom->pose.pose.orientation.z = q.z();
    odom->pose.pose.orientation.w = q.w();
    
    // Estimate orientation from trajectory
    if(iidreLastPosition.x == 0.0)
      iidreLastPosition.x = odom->pose.pose.position.x;

    if(iidreLastPosition.y == 0.0)
      iidreLastPosition.y = odom->pose.pose.position.y;

    float deltax = odom->pose.pose.position.x - iidreLastPosition.x;
    float deltay = odom->pose.pose.position.y - iidreLastPosition.y;
    float ds = sqrt(deltax*deltax + deltay*deltay);

    // Publish difference between heading estimate from iidre and imu
    std_msgs::msg::Float64 var;
    var.data = headingIidre*180/3.1415;
    this->pubIidreHeading->publish(var);

    // Save last values in order to estimate orientation
    iidreLastPosition.x = odom->pose.pose.position.x;
    iidreLastPosition.y = odom->pose.pose.position.y;

    if((fabs(ds) > 1.0) || !distanceValidity(3))
      return ;

    odom->twist.twist.angular.z = - odom->twist.twist.angular.z; 

    // Republish new odometry value with calculated covariances
    this->pubIidreOdom->publish(*odom);
  }

  void odomMarvel1Callback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    // Check current quality 
    //if(marvel1Quality != 100.0)
      //return ;

    odom->child_frame_id = "Hight_Front_Marvel1_Link";
    odom->header.frame_id = "map";

    // Odometry covariances as the calculated values
    odom->pose.covariance = calcMarvel.pose.covariance;

    // Save current values to remove offsets
    marvelmindBeacon1.bodyPose.x = 0.68947;
    marvelmindBeacon1.bodyPose.y = 0.282;

    // Calculate offsets 
    float OFFSET_X = marvelmindBeacon1.bodyPose.x*cos(headingOdom) + marvelmindBeacon1.bodyPose.y*sin(headingOdom);
    float OFFSET_Y = marvelmindBeacon1.bodyPose.y*cos(headingOdom) + marvelmindBeacon1.bodyPose.x*sin(headingOdom);

    marvel1Buffer.x->push(odom->pose.pose.position.x);
    marvel1Buffer.y->push(odom->pose.pose.position.y);

    // Remove position offset due to beacon placement in the robot
    odom->pose.pose.position.x = marvel1Buffer.x->mean() - OFFSET_X; 
    odom->pose.pose.position.y = marvel1Buffer.y->mean() - OFFSET_Y; 
    odom->pose.pose.position.z = 0.0; 
    
    // Estimate orientation from trajectory
    headingMarvel1 = orientationFromMarvel();

    // Save orientation data in odom message
    tf2::Quaternion q;
    q.setRPY(0, 0, headingMarvel1);  // Create this quaternion from roll/pitch/yaw (in radians)
    odom->pose.pose.orientation.x = q.x();
    odom->pose.pose.orientation.y = q.y();
    odom->pose.pose.orientation.z = q.z();
    odom->pose.pose.orientation.w = q.w();

    // Verify jumps
    if(marvel1LastPosition.x == 0.0)
      marvel1LastPosition.x = odom->pose.pose.position.x;

    if(marvel1LastPosition.y == 0.0)
      marvel1LastPosition.y = odom->pose.pose.position.y;

    float deltax = odom->pose.pose.position.x - marvel1LastPosition.x;
    float deltay = odom->pose.pose.position.y - marvel1LastPosition.y;
    float ds = sqrt(deltax*deltax + deltay*deltay);
    
    // Save last values in order to estimate orientation
    marvel1LastPosition.x = odom->pose.pose.position.x;
    marvel1LastPosition.y = odom->pose.pose.position.y;

    if((fabs(ds) > 1.0) || !distanceValidity(1))
      return ;

    // Republish new odometry value with calculated covariances
    this->pubMarvel1Odom->publish(*odom);
  }

  void odomMarvel2Callback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    //RCLCPP_INFO(this->get_logger(), "Publishing odom with covariance  %f", marvel2Quality);	
    // Check current quality 
    //if(marvel2Quality != 100.0)
      //return ;

    odom->child_frame_id = "Left_Botton_Marvel2_Link";
    odom->header.frame_id = "map";

    // Odometry covariances as the calculated values
    odom->pose.covariance = calcMarvel.pose.covariance;

    // Save current values to remove offsets
    marvelmindBeacon2.bodyPose.x = -0.63858;
    marvelmindBeacon2.bodyPose.y = -0.482;

    // Calculate offsets 
    float OFFSET_X = marvelmindBeacon2.bodyPose.x*cos(headingOdom) - marvelmindBeacon2.bodyPose.y*sin(headingOdom);
    float OFFSET_Y = marvelmindBeacon2.bodyPose.y*cos(headingOdom) + marvelmindBeacon2.bodyPose.x*sin(headingOdom);

    // Save values in buffer in order to filter
    marvel2Buffer.x->push(odom->pose.pose.position.x);
    marvel2Buffer.y->push(odom->pose.pose.position.y);

    // Remove position offset due to beacon placement in the robot
    odom->pose.pose.position.x = marvel2Buffer.x->mean() - OFFSET_X; 
    odom->pose.pose.position.y = marvel2Buffer.y->mean() - OFFSET_Y; 
    odom->pose.pose.position.z = 0.0; 
    
    // Estimate orientation from trajectory
    headingMarvel2 = orientationFromMarvel();

    // Save orientation data in odom message
    tf2::Quaternion q;
    q.setRPY(0, 0, headingMarvel2);  // Create this quaternion from roll/pitch/yaw (in radians)
    odom->pose.pose.orientation.x = q.x();
    odom->pose.pose.orientation.y = q.y();
    odom->pose.pose.orientation.z = q.z();
    odom->pose.pose.orientation.w = q.w();

    // Verify jumps
    if(marvel2LastPosition.x == 0.0)
      marvel2LastPosition.x = odom->pose.pose.position.x;

    if(marvel2LastPosition.y == 0.0)
      marvel2LastPosition.y = odom->pose.pose.position.y;

    float deltax = odom->pose.pose.position.x - marvel2LastPosition.x;
    float deltay = odom->pose.pose.position.y - marvel2LastPosition.y;
    float ds = sqrt(deltax*deltax + deltay*deltay);

    // Save last values in order to estimate orientation
    marvel2LastPosition.x = odom->pose.pose.position.x;
    marvel2LastPosition.y = odom->pose.pose.position.y;

    if((fabs(ds) > 1.0) || !distanceValidity(2))
      return ;

    // Republish new odometry value with calculated covariances
    this->pubMarvel2Odom->publish(*odom);
  }

  void imuCallback(const sensor_msgs::msg::Imu::SharedPtr imu)
  {
    // ------- Initializations
    builtin_interfaces::msg::Time currentTime = imu->header.stamp;
    imu->header.frame_id = "IMU_F_LINK";

    // ------- Check time difference since the last measurement in order de control the publication frequency
    //double dt = (double)(currentTime.sec - imuLastMeas.sec) + (double)(currentTime.nanosec - imuLastMeas.nanosec)/1000000000.0f;
    //double currentFrequency = 0;

    //if(dt != 0) currentFrequency = 1/dt;

    // ------- Verify if the robot is moving in order to calculate offsets
    if(!is_moving)
    {
      imuOffset.accelerometer.x->push(imu->linear_acceleration.x);
      imuOffset.accelerometer.y->push(imu->linear_acceleration.y);
      imuOffset.accelerometer.z->push(imu->linear_acceleration.z);

      imuOffset.gyro.x->push(imu->angular_velocity.x);
      imuOffset.gyro.y->push(imu->angular_velocity.y);
      imuOffset.gyro.z->push(imu->angular_velocity.z);
    }

    // ------- Update covariances	
    imu->angular_velocity_covariance = calcImu.angular_velocity_covariance;
    imu->linear_acceleration_covariance = calcImu.linear_acceleration_covariance;
    imu->orientation_covariance = calcImu.orientation_covariance;

    if(imu->angular_velocity.z > 0.01)
    {
      //calcOdom.pose.covariance[0] = 900;
      //calcOdom.pose.covariance[7] = 900;

      //calcOdom.twist.covariance[0] = 0.1;
      //calcOdom.twist.covariance[7] = 0.1;
      //calcOdom.twist.covariance[35] = 0.1;

    } else
    {
      //calcOdom.pose.covariance[0] = 500;
      //calcOdom.pose.covariance[7] = 500;

      //calcOdom.twist.covariance[0] = 0.1;
      //calcOdom.twist.covariance[7] = 0.1;
      //calcOdom.twist.covariance[35] = 0.001; 
    }

    // ------- Remove offsets
    imu->linear_acceleration.x = imu->linear_acceleration.x - imuOffset.accelerometer.x->mean();
    imu->linear_acceleration.y = imu->linear_acceleration.y - imuOffset.accelerometer.y->mean();
    imu->linear_acceleration.z = imu->linear_acceleration.z - imuOffset.accelerometer.z->mean();  
    
    imu->angular_velocity.x = imu->angular_velocity.x - imuOffset.gyro.x->mean();
    imu->angular_velocity.y = imu->angular_velocity.y - imuOffset.gyro.y->mean();
    imu->angular_velocity.z = imu->angular_velocity.z - imuOffset.gyro.z->mean();  
    
    // ------- Save orientation from IMU measurement
    tf2::Quaternion q(imu->orientation.x,
                      imu->orientation.y,
                      imu->orientation.z,
                      imu->orientation.w);

    tf2::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    // There is a offset of pi rad
    headingImu = yaw - PI;

    if(headingImu > PI)
      headingImu -= 2*PI;
    else if(headingImu < -PI)
      headingImu += 2*PI;

    std_msgs::msg::Float64 var;
    var.data = headingImu*180/PI;
    this->pubImuHeading->publish(var);

    //RCLCPP_INFO(this->get_logger(), "%f  %f \n", yaw, headingImu);	

    // ------- Publish
    this->pubImu->publish(*imu);
    imuLastMeas = currentTime; 
  }

  void tfCallback(tf2_msgs::msg::TFMessage::SharedPtr tf_msg)
    {
      int num_tfs = tf_msg->transforms.size();
      std_msgs::msg::Int32MultiArray array;
      array.data = {0, 0, 0, 0, 0, 0, 0, 0};
      bool is_new_data = true;
      std_msgs::msg::Float64 var;
      var.data = 0.0;
    
      for (int i = 0; i < num_tfs; i++)
      {        
        if (tf_msg->transforms.at(i).child_frame_id == "1565091E")
        {
          array.data.at(0) = tf_msg->transforms.at(i).transform.rotation.x; //FP_PWR_LVL: first-path (*1000) power in dBm
          array.data.at(1) = tf_msg->transforms.at(i).transform.rotation.y; //IDIFF: LOS/NLOS communication indicator, w/ unit
          
          var.data = (float)tf_msg->transforms.at(i).transform.rotation.y;
          //pubQualBeacon1->publish(var);
        } 
        else if (tf_msg->transforms.at(i).child_frame_id == "15648BA2"){
          array.data.at(2) = tf_msg->transforms.at(i).transform.rotation.x;  
          array.data.at(3) = tf_msg->transforms.at(i).transform.rotation.y;

          var.data = tf_msg->transforms.at(i).transform.rotation.y;
          //pubQualBeacon2->publish(var);
        } 
        else if (tf_msg->transforms.at(i).child_frame_id == "55644820"){
          array.data.at(4) = tf_msg->transforms.at(i).transform.rotation.x;
          array.data.at(5) = tf_msg->transforms.at(i).transform.rotation.y;

          var.data = tf_msg->transforms.at(i).transform.rotation.y;
          //pubQualBeacon3->publish(var); 
        } 
        else if (tf_msg->transforms.at(i).child_frame_id == "55650120"){
          array.data.at(6) = tf_msg->transforms.at(i).transform.rotation.x;
          array.data.at(7) = tf_msg->transforms.at(i).transform.rotation.y;  

          var.data = tf_msg->transforms.at(i).transform.rotation.y;
          //pubQualBeacon4->publish(var);
        
        }
        else if (tf_msg->transforms.at(i).child_frame_id == "hedge"){
          tf_msg->transforms.erase(tf_msg->transforms.cbegin()+i);
        } 
        else if (tf_msg->transforms.at(i).child_frame_id == "uwb"){
          tf_msg->transforms.erase(tf_msg->transforms.cbegin()+i);
        } 
        else if (tf_msg->transforms.at(i).header.frame_id == "odom"){
          tf_msg->transforms.erase(tf_msg->transforms.cbegin()+i);
        } 
        else
        {
          is_new_data = false;
        }
      }

      if (is_new_data)
      {
        //pubIidreQuality->publish(array);
      }

      pubTf->publish(*tf_msg);
    }

  void staticTfCallback(tf2_msgs::msg::TFMessage::SharedPtr tf_msg)
    {
      int num_tfs = tf_msg->transforms.size();

      for (int i = 0; i < num_tfs; i++)
      {        
        if (tf_msg->transforms.at(i).child_frame_id == "hedge")
        {
          marvelmindBeacon1.bodyPose.x = tf_msg->transforms.at(i).transform.translation.x;
          marvelmindBeacon1.bodyPose.y = tf_msg->transforms.at(i).transform.translation.y;
          marvelmindBeacon1.bodyPose.z = 0.0;
        } 
        else if(tf_msg->transforms.at(i).child_frame_id == "uwb")
        {
          iidreBeacon1.bodyPose.x = tf_msg->transforms.at(i).transform.translation.x;
          iidreBeacon1.bodyPose.y = tf_msg->transforms.at(i).transform.translation.y; 
          iidreBeacon1.bodyPose.z = 0.0;
        }
      }
    }
};

/**
 * Node for utilities necessary to evaluate scooby localization performance
 */
int main(int argc, char **argv)
{
	
  // initialize ROS node
  rclcpp::init(argc, argv);

  rclcpp::spin(std::make_shared<LocalizationNode>());
  rclcpp::shutdown();

  return 0;
}
