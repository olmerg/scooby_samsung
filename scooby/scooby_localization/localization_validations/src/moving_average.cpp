#include "moving_average.hpp"
#include <iostream>

MovingAverage::MovingAverage(int n) 
{
	sum=0;
	ssq=0;
	q = new boost::circular_buffer<double>(n);
}

MovingAverage::~MovingAverage()
{
	delete this->q;
}

void MovingAverage::push(double v) {
	if (q->size() == q->capacity()) 
	{
		double t=q->front();
		sum-=t;
		ssq-=t*t;
		q->pop_front();
	}

	q->push_back(v);
	sum+=v;
	ssq+=v*v;
}

float MovingAverage::size() 
{
	return q->size();
}

float MovingAverage::mean() 
{
	float size_ = size();
	if(size_ == 0) return 0.0;

	return sum/size();
}

float MovingAverage::std() 
{
	float size_ = size();
	if(size_ == 0.0) return 0.0;

	float arg1 = (1/size_) * ssq;
	float arg2 = ((1/size_) * sum) * ((1/size_) * sum);

	return sqrt(fabs(arg1 - arg2));
}


