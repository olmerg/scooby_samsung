#include "rclcpp/rclcpp.hpp"
#include "moving_average.hpp"
#include <nav_msgs/msg/odometry.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/buffer_interface.h>
#include <tf2_ros/visibility_control.h>
#include <tf2/buffer_core.h>
#include <tf2/time.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <visualization_msgs/msg/marker_array.hpp>


#include <chrono>

using namespace std::chrono_literals;

#define ROS_NODE_NAME "statistics_node"
#define PI 3.1415

class StatisticsNode : public rclcpp::Node
{
  public:
    StatisticsNode()
    : Node(ROS_NODE_NAME)
    {
      auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());

		// ************** Initializations **************
		setParkingDefault();
		setTotalDefault();
		setSpots();
      
      // ************** Declare subscribers **************
			subParking = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"/parking_message", default_qos, std::bind(&StatisticsNode::parkingCallback, this, std::placeholders::_1)); 

			subOdom = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"/odom_with_covariance", default_qos, std::bind(&StatisticsNode::odomCallback, this, std::placeholders::_1));

			subIidreOdom = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"/iidre_with_covariance", 10, std::bind(&StatisticsNode::odomIidreCallback, this, std::placeholders::_1));

			subMarvel1Odom = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"marvel1_with_covariance", 10, std::bind(&StatisticsNode::odomMarvel1Callback, this, std::placeholders::_1));

			subMarvel2Odom = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"marvel2_with_covariance", 10, std::bind(&StatisticsNode::odomMarvel2Callback, this, std::placeholders::_1));

			subKfOdom = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"kf_odom", 10, std::bind(&StatisticsNode::kfOdomCallback, this, std::placeholders::_1));

			subKfGlob = this->create_subscription
			<nav_msgs::msg::Odometry>(
			"kf_global", 10, std::bind(&StatisticsNode::kfGlobalCallback, this, std::placeholders::_1));

			subRvizMarker = this->create_subscription
			<visualization_msgs::msg::MarkerArray>(
			"graph_visualization", 10, std::bind(&StatisticsNode::markerCallback, this, std::placeholders::_1));
		}

  private:
    //  ************** DATA TYPES **************
    struct averagedAxis
    {
			MovingAverage *linear;
			MovingAverage *angular;
			MovingAverage *x;
			MovingAverage *y;
    };

    //  ************** PUBLISHERS AND SUBSCRIBERS **************

    // --- Subscribers
		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subParking;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subOdom;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subIidreOdom;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subMarvel1Odom;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subMarvel2Odom;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subKfOdom;

		rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subKfGlob;

		rclcpp::Subscription<visualization_msgs::msg::MarkerArray>::SharedPtr subRvizMarker;

    //  ************** AUX VARIABLES **************
		geometry_msgs::msg::Pose iidreMsg;
		geometry_msgs::msg::Pose marvel1Msg;
		geometry_msgs::msg::Pose marvel2Msg;
		geometry_msgs::msg::Pose odomMsg;
		geometry_msgs::msg::Pose kOdomMsg;
		geometry_msgs::msg::Pose kGlobMsg;
		geometry_msgs::msg::Pose slamMsg;

		averagedAxis parkingIidreError;
		averagedAxis parkingMarvel1Error;
		averagedAxis parkingMarvel2Error;
		averagedAxis parkingOdomError;
		averagedAxis parkingKOdomError;
		averagedAxis parkingKGloblError;
		averagedAxis parkingSlamError;

		averagedAxis totalIidreError;
		averagedAxis totalMarvel1Error;
		averagedAxis totalMarvel2Error;
		averagedAxis totalOdomError;
		averagedAxis totalKOdomError;
		averagedAxis totalKGloblError;
		averagedAxis totalSlamError;

		int last_spot = 1;
		float spot_pose[8][3];
		float deg2rad = PI/180.0;
		float rad2deg = 1/deg2rad;
		float ROBOT_X_SIZE = 1.415;
		float ROBOT_Y_SIZE = 0.855; 
    int last_marker_size = 0;

  //  ************** AUX METHODS **************
	void setSpots()
	{
		float OFFSET_X = this->ROBOT_X_SIZE/2.0f;
		float OFFSET_Y = this->ROBOT_Y_SIZE/2.0f;

		spot_pose[0][0] = 0.0 - OFFSET_X;		  spot_pose[0][1] = 1.5 - OFFSET_Y;		spot_pose[0][2] = 180.0*deg2rad; 	
		spot_pose[1][0] = -2.0 - OFFSET_X;		spot_pose[1][1] = 1.5 - OFFSET_Y;		spot_pose[1][2] = 180.0*deg2rad; 	
		spot_pose[2][0] = 2.50 + OFFSET_X;		spot_pose[2][1] = 2.5 + OFFSET_Y;		spot_pose[2][2] = 0.0*deg2rad; 	
		spot_pose[3][0] = 4.00 + OFFSET_Y;		spot_pose[3][1] = 6.0 - OFFSET_X;		spot_pose[3][2] = 270.0*deg2rad; 	
		spot_pose[4][0] = 5.00 - OFFSET_Y;		spot_pose[4][1] = 0.0 + OFFSET_X;		spot_pose[4][2] = 90.0*deg2rad; 	
		spot_pose[5][0] = 12.00 + OFFSET_X;		spot_pose[5][1] = 4.7 + OFFSET_Y;		spot_pose[5][2] = 0.0*deg2rad; 	
		spot_pose[6][0] = 18.00 + OFFSET_X;		spot_pose[6][1] = 0.4 + OFFSET_Y;		spot_pose[6][2] = 0.0*deg2rad; 	
		spot_pose[7][0] = 9.00 - OFFSET_X;		spot_pose[7][1] = 2.00 - OFFSET_Y;		spot_pose[7][2] = 180.0*deg2rad; 	
	}

	float linear_error(float x1, float y1, float x2, float y2)
  {
		return sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
  }

	float angular_error(float x1, float x2)
  {
		float diff = fabs(x2 - x1);
		if(diff > PI)
      diff -= 2*PI;
    else if(diff < -PI)
      diff += 2*PI;

		return diff;
  }

	void initialize_axis(int size, averagedAxis* axis)
  {
		axis->linear = new MovingAverage(size);
		axis->angular = new MovingAverage(size);
		axis->x = new MovingAverage(size);
		axis->y = new MovingAverage(size);
  }

	void printParkingErrors(int current_spot)
	{
		RCLCPP_INFO(this->get_logger(), " ");
		RCLCPP_INFO(this->get_logger(), "Errors for spot %d ------------------------", last_spot);	
		RCLCPP_INFO(this->get_logger(), "Odometry: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingOdomError.linear->mean(), parkingOdomError.linear->std(), parkingOdomError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingOdomError.angular->mean()*rad2deg, parkingOdomError.angular->std()*rad2deg, parkingOdomError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingOdomError.x->mean(), parkingOdomError.x->std()*rad2deg, parkingOdomError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingOdomError.y->mean(), parkingOdomError.y->std()*rad2deg, parkingOdomError.y->size());

		RCLCPP_INFO(this->get_logger(), "IIDRE: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingIidreError.linear->mean(), parkingIidreError.linear->std(), parkingIidreError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingIidreError.angular->mean()*rad2deg, parkingIidreError.angular->std()*rad2deg, parkingIidreError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingIidreError.x->mean(), parkingIidreError.x->std(), parkingIidreError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingIidreError.y->mean(), parkingIidreError.y->std(), parkingIidreError.y->size());
		
		RCLCPP_INFO(this->get_logger(), "Marvel 1: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel1Error.linear->mean(), parkingMarvel1Error.linear->std(), parkingMarvel1Error.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingMarvel1Error.angular->mean()*rad2deg, parkingMarvel1Error.angular->std()*rad2deg, parkingMarvel1Error.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel1Error.x->mean(), parkingMarvel1Error.x->std(), parkingMarvel1Error.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel1Error.y->mean(), parkingMarvel1Error.y->std(), parkingMarvel1Error.y->size());

		RCLCPP_INFO(this->get_logger(), "Marvel 2: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel2Error.linear->mean(), parkingMarvel2Error.linear->std(), parkingMarvel2Error.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingMarvel2Error.angular->mean()*rad2deg, parkingMarvel2Error.angular->std()*rad2deg, parkingMarvel2Error.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel2Error.x->mean(), parkingMarvel2Error.x->std(), parkingMarvel2Error.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingMarvel2Error.y->mean(), parkingMarvel2Error.y->std(), parkingMarvel2Error.y->size());

		RCLCPP_INFO(this->get_logger(), "Slam: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingSlamError.linear->mean(), parkingSlamError.linear->std(), parkingSlamError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingSlamError.angular->mean()*rad2deg, parkingSlamError.angular->std()*rad2deg, parkingSlamError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingSlamError.x->mean(), parkingSlamError.x->std(), parkingSlamError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingSlamError.y->mean(), parkingSlamError.y->std(), parkingSlamError.y->size());

		RCLCPP_INFO(this->get_logger(), "Kalman Continuous: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKOdomError.linear->mean(), parkingKOdomError.linear->std(), parkingKOdomError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingKOdomError.angular->mean()*rad2deg, parkingKOdomError.angular->std()*rad2deg, parkingKOdomError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKOdomError.x->mean(), parkingKOdomError.x->std(), parkingKOdomError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKOdomError.y->mean(), parkingKOdomError.y->std(), parkingKOdomError.y->size());

		RCLCPP_INFO(this->get_logger(), "Kalman Global: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKGloblError.linear->mean(), parkingKGloblError.linear->std(), parkingKGloblError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", parkingKGloblError.angular->mean()*rad2deg, parkingKGloblError.angular->std()*rad2deg, parkingKGloblError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKGloblError.x->mean(), parkingKGloblError.x->std(), parkingKGloblError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", parkingKGloblError.y->mean(), parkingKGloblError.y->std(), parkingKGloblError.y->size());
	}

	void printTotalErrors()
	{
		RCLCPP_INFO(this->get_logger(), " ");
		RCLCPP_INFO(this->get_logger(), "Total Errors ------------------------------------------ ");	
		RCLCPP_INFO(this->get_logger(), "Odometry: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalOdomError.linear->mean(), totalOdomError.linear->std(), totalOdomError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalOdomError.angular->mean()*rad2deg, totalOdomError.angular->std()*rad2deg, totalOdomError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalOdomError.x->mean(), totalOdomError.x->std(), totalOdomError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalOdomError.y->mean(), totalOdomError.y->std(), totalOdomError.y->size());

		RCLCPP_INFO(this->get_logger(), "IIDRE: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalIidreError.linear->mean(), totalIidreError.linear->std(), totalIidreError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalIidreError.angular->mean()*rad2deg, totalIidreError.angular->std()*rad2deg, totalIidreError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalIidreError.x->mean(), totalIidreError.x->std(), totalIidreError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalIidreError.y->mean(), totalIidreError.y->std(), totalIidreError.y->size());
		
		RCLCPP_INFO(this->get_logger(), "Marvel 1: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel1Error.linear->mean(), totalMarvel1Error.linear->std(), totalMarvel1Error.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalMarvel1Error.angular->mean()*rad2deg, totalMarvel1Error.angular->std()*rad2deg, totalMarvel1Error.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel1Error.x->mean(), totalMarvel1Error.x->std(), totalMarvel1Error.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel1Error.y->mean(), totalMarvel1Error.y->std(), totalMarvel1Error.y->size());

		RCLCPP_INFO(this->get_logger(), "Marvel 2: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel2Error.linear->mean(), totalMarvel2Error.linear->std(), totalMarvel2Error.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalMarvel2Error.angular->mean()*rad2deg, totalMarvel2Error.angular->std()*rad2deg, totalMarvel2Error.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel2Error.x->mean(), totalMarvel2Error.x->std(), totalMarvel2Error.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalMarvel2Error.y->mean(), totalMarvel2Error.y->std(), totalMarvel2Error.y->size());

		RCLCPP_INFO(this->get_logger(), "Slam: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalSlamError.linear->mean(), totalSlamError.linear->std(), totalSlamError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalSlamError.angular->mean()*rad2deg, totalSlamError.angular->std()*rad2deg, totalSlamError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalSlamError.x->mean(), totalSlamError.x->std(), totalSlamError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalSlamError.y->mean(), totalSlamError.y->std(), totalSlamError.y->size());

		RCLCPP_INFO(this->get_logger(), "Kalman Continuous: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKOdomError.linear->mean(), totalKOdomError.linear->std(), totalKOdomError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalKOdomError.angular->mean()*rad2deg, totalKOdomError.angular->std()*rad2deg, totalKOdomError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKOdomError.x->mean(), totalKOdomError.x->std(), totalKOdomError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKOdomError.y->mean(), totalKOdomError.y->std(), totalKOdomError.y->size());

		RCLCPP_INFO(this->get_logger(), "Kalman Global: ");
		RCLCPP_INFO(this->get_logger(), "	Linear->  Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKGloblError.linear->mean(), totalKGloblError.linear->std(), totalKGloblError.linear->size());
		RCLCPP_INFO(this->get_logger(), "	Angular-> Mean: %.03f   [º] STD: %.03f   [º] Samples: %.03f", totalKGloblError.angular->mean()*rad2deg, totalKGloblError.angular->std()*rad2deg, totalKGloblError.angular->size());
		RCLCPP_INFO(this->get_logger(), "	X->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKGloblError.x->mean(), totalKGloblError.x->std(), totalKGloblError.x->size());
		RCLCPP_INFO(this->get_logger(), "	Y->       Mean: %.03f   [m] STD: %.03f   [m] Samples: %.03f", totalKGloblError.y->mean(), totalKGloblError.y->std(), totalKGloblError.y->size());
	}

	void setParkingDefault(){
		// Initialize buffers
		initialize_axis(100, &parkingIidreError);
		initialize_axis(100, &parkingOdomError);
		initialize_axis(100, &parkingMarvel1Error);
		initialize_axis(100, &parkingMarvel2Error);
		initialize_axis(100, &parkingKOdomError);
		initialize_axis(100, &parkingKGloblError);
		initialize_axis(100, &parkingSlamError);
  }

	void setTotalDefault(){
		// Initialize buffers
		initialize_axis(1000, &totalIidreError);
		initialize_axis(1000, &totalOdomError);
		initialize_axis(1000, &totalMarvel1Error);
		initialize_axis(1000, &totalMarvel2Error);
		initialize_axis(1000, &totalKOdomError);
		initialize_axis(1000, &totalKGloblError);
		initialize_axis(1000, &totalSlamError);
  }

	void calculateErrors(geometry_msgs::msg::Pose msg, averagedAxis* buffer, averagedAxis* total_buffer, float * spot_pose)
	{
		tf2::Quaternion q(msg.orientation.x,
											msg.orientation.y,
											msg.orientation.z,
											msg.orientation.w);

		tf2::Matrix3x3 m(q);
		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);

		float l_error = linear_error(msg.position.x, msg.position.y, spot_pose[0], spot_pose[1]);
		float ang_error = angular_error(yaw, spot_pose[2]);
		
		//RCLCPP_INFO(this->get_logger(), "Yaw %f  Pose %f   Error %f", yaw, spot_pose[2], ang_error);

		buffer->linear->push(l_error);
		buffer->angular->push(ang_error);
		buffer->x->push(msg.position.x - spot_pose[0]);
		buffer->y->push(msg.position.y - spot_pose[1]);

		total_buffer->linear->push(l_error);
		total_buffer->angular->push(ang_error);
		total_buffer->x->push(msg.position.x - spot_pose[0]);
		total_buffer->y->push(msg.position.y - spot_pose[1]);
	}

  //  ************** CALLBACKS **************

	void markerCallback( const visualization_msgs::msg::MarkerArray::SharedPtr marker)
	{
		std::vector<visualization_msgs::msg::Marker> marker_msg = marker->markers;
		int current_marker_size = marker_msg.size();
		if(last_marker_size != current_marker_size)
			slamMsg = marker_msg[current_marker_size - 1].pose;

		last_marker_size = current_marker_size;
	}

  void parkingCallback(const nav_msgs::msg::Odometry::SharedPtr parking)
  {
		// Verify if is the same spot as the last iteration 
		int current_spot = parking->twist.twist.linear.z;

		if(last_spot == current_spot)
		{
			// Calculate errors for each system
			// -- Odom
			calculateErrors(odomMsg, &parkingOdomError, &totalOdomError, spot_pose[current_spot - 1]);

			// -- Iidre
			calculateErrors(iidreMsg, &parkingIidreError, &totalIidreError, spot_pose[current_spot - 1]);

			// -- Marvel 1
			calculateErrors(marvel1Msg, &parkingMarvel1Error, &totalMarvel1Error, spot_pose[current_spot - 1]);

			// -- Marvel 2
			calculateErrors(marvel2Msg, &parkingMarvel2Error, &totalMarvel2Error, spot_pose[current_spot - 1]);

			// -- Slam 
			calculateErrors(slamMsg, &parkingSlamError, &totalSlamError, spot_pose[current_spot - 1]);

			// -- Kalman Odom
			calculateErrors(kOdomMsg, &parkingKOdomError, &totalKOdomError, spot_pose[current_spot - 1]);

			// -- Kalman Global
			calculateErrors(kGlobMsg, &parkingKGloblError, &totalKGloblError, spot_pose[current_spot - 1]);
		} else 
		{
			// Print current errors
			printParkingErrors(current_spot);

			// Reset parking buffers
			setParkingDefault();

			// Print total errors
			if(last_spot >= 7)
			{
				printTotalErrors();
			}
		}

		// Update last spot value
			last_spot = current_spot; 
  }

  void odomCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->odomMsg = odom->pose.pose;

		//RCLCPP_INFO(this->get_logger(), "[NEW ODOM MESSAGE] Yaw %f  Yaw2 %f", yaw, yaw2);
  }

  void odomIidreCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->iidreMsg =  odom->pose.pose;
  }

  void odomMarvel1Callback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->marvel1Msg =  odom->pose.pose;
  }

  void odomMarvel2Callback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->marvel2Msg =  odom->pose.pose;
  }

	void kfOdomCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->kOdomMsg =  odom->pose.pose;
  }

	void kfGlobalCallback(const nav_msgs::msg::Odometry::SharedPtr odom)
  {
    this->kGlobMsg =  odom->pose.pose;
  }

};

/**
 * Node for utilities necessary to evaluate scooby localization performance
 */
int main(int argc, char **argv)
{
	
  // initialize ROS node
  rclcpp::init(argc, argv);

  rclcpp::spin(std::make_shared<StatisticsNode>());
  rclcpp::shutdown();

  return 0;
}
