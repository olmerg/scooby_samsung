#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <tf2/LinearMath/Quaternion.h>
#include <visualization_msgs/msg/marker.hpp>
#include <tf2/time.h>
#include <visualization_msgs/msg/marker_array.hpp>
#include "std_msgs/msg/float32.hpp"


#include <chrono>

using namespace std::chrono_literals;

#define ROS_NODE_NAME "localization_eval"
#define PI 3.1415

class LocalizationEval : public rclcpp::Node
{
  public:
    LocalizationEval()
    : Node(ROS_NODE_NAME)
    { 
      // ************** Declare publishers for rviz visualization **************
      pubRvizMarker = this->create_publisher<visualization_msgs::msg::MarkerArray>("visualization_marker_array", 1);

      // ************** Publish markers **************
      setKnownPositions();
      timer_ = this->create_wall_timer(
      500ms, std::bind(&LocalizationEval::publishMarkers, this));      
    }

  private:
    rclcpp::TimerBase::SharedPtr timer_;
    visualization_msgs::msg::MarkerArray marker_array;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr pubRvizMarker;
    
    float ROBOT_X_SIZE = 1.415;
    float ROBOT_Y_SIZE = 0.855; 

    void addMarker(uint8_t address, float x, float y, float z, float orientation_angle) 
    {      
      RCLCPP_INFO(this->get_logger(), "Publishing known positions");	

      visualization_msgs::msg::Marker marker;
      visualization_msgs::msg::Marker circle;

      tf2::Quaternion q;
      q.setRPY(0, 0, orientation_angle);  // Create this quaternion from roll/pitch/yaw (in radians)
      
      // Set the frame ID and timestamp.  See the TF tutorials for information on these.
      marker.header.frame_id = "/map";
      marker.header.stamp = this->now();
      circle.header.frame_id = "/map";
      circle.header.stamp = this->now();

      // Set the namespace and id for this marker.  This serves to create a unique ID
      // Any marker sent with the same namespace and id will overwrite the old one
      marker.ns = "basic_shapes";
      marker.id = address;
      circle.ns = "basic_shapes";
      circle.id = address + 10;
      // Set the marker type
      marker.type = visualization_msgs::msg::Marker::CUBE;
      circle.type = visualization_msgs::msg::Marker::CYLINDER;
    
      // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
      marker.action = visualization_msgs::msg::Marker::ADD;
      circle.action = visualization_msgs::msg::Marker::ADD;
      
      // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
      marker.pose.position.x = x;
      marker.pose.position.y = y;
      marker.pose.position.z = z;
      marker.pose.orientation.x = q.x();
      marker.pose.orientation.y = q.y();
      marker.pose.orientation.z = q.z();
      marker.pose.orientation.w = q.w();

      circle.pose.position.x = x;
      circle.pose.position.y = y;
      circle.pose.position.z = z;
      circle.pose.orientation.x = q.x();
      circle.pose.orientation.y = q.y();
      circle.pose.orientation.z = q.z();
      circle.pose.orientation.w = q.w();
      // Set the scale of the marker -- 1x1x1 here means 1m on a side
      marker.scale.x = this->ROBOT_X_SIZE;
      marker.scale.y = this->ROBOT_Y_SIZE;
      marker.scale.z = 0.01;

      circle.scale.x = 0.1;
      circle.scale.y = 0.1;
      circle.scale.z = 0.01;
      // Set the color -- be sure to set alpha to something non-zero!
      marker.color.r = 0.0f;
      marker.color.g = 0.0f;
      marker.color.b = 1.0f;

      circle.color.r = 1.0f;
      circle.color.g = 0.0f;
      circle.color.b = 0.0f;
      // Set the transparency
      marker.color.a = 0.4;
      marker.lifetime = rclcpp::Duration(500);

      circle.color.a = 1.0;
      circle.lifetime = rclcpp::Duration(500);

      marker_array.markers.push_back(marker);      
      marker_array.markers.push_back(circle);     
    }

    void setKnownPositions() 
    { 
			RCLCPP_INFO(this->get_logger(), "Publishing known positions");	
      float deg2rad = PI/180;
      float OFFSET_X = this->ROBOT_X_SIZE/2.0f;
      float OFFSET_Y = this->ROBOT_Y_SIZE/2.0f;

      addMarker(1,  0.0 - OFFSET_X, 1.5 - OFFSET_Y, 0.0, 180.0*deg2rad);
      addMarker(2, -2.0 - OFFSET_X , 1.5 - OFFSET_Y, 0.0, 180.0*deg2rad);
      addMarker(3, 2.50 + OFFSET_X, 2.5 + OFFSET_Y, 0.0, 0.0);
      addMarker(4, 4.00 + OFFSET_Y, 6.0 - OFFSET_X, 0.0, 270.0*deg2rad);
      addMarker(5, 5.00 - OFFSET_Y, 0.0 + OFFSET_X, 0.0, 90.0*deg2rad);
      addMarker(6, 12.00 + OFFSET_X, 4.7 + OFFSET_Y, 0.0, 0.0*deg2rad);
      addMarker(7, 18.00 + OFFSET_X, 0.4 + OFFSET_Y, 0.0, 0.0*deg2rad);
      addMarker(8, 9.00 - OFFSET_X, 2.00 - OFFSET_Y, 0.0, 180.0*deg2rad);
    }
    
    void publishMarkers() 
    {      
      this->pubRvizMarker->publish(marker_array);
    }
};

/**
 * Node for utilities necessary to evaluate scooby localization performance
 */
int main(int argc, char **argv)
{
	
  // initialize ROS node
  rclcpp::init(argc, argv);

  rclcpp::spin(std::make_shared<LocalizationEval>());
  rclcpp::shutdown();

  return 0;
}
