#include <boost/circular_buffer.hpp>
#include <math.h>   

class MovingAverage 
{
    public:
        boost::circular_buffer<double> *q;
        float sum;
        float ssq;
        
        MovingAverage(int n);
        ~MovingAverage(); 
        void push(double v);
        float size();
        float mean();
        float std();
};