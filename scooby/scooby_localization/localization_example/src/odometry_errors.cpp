#include "odometry_errors.hpp"

using std::placeholders::_1;
using namespace std::chrono_literals;

OdometryErrors::OdometryErrors()
: Node("odometry_errors_node")
{
  nh_ = std::shared_ptr<::rclcpp::Node>(this, [](::rclcpp::Node *) {});
  cont_sub = nh_->create_subscription<nav_msgs::msg::Odometry>(
    "odometry/cont_filtered",
    1,
    std::bind(&OdometryErrors::contCallback, this, std::placeholders::_1));
  
  discr_sub = nh_->create_subscription<nav_msgs::msg::Odometry>(
    "odometry/discr_filtered",
    1,
    std::bind(&OdometryErrors::discrCallback, this, std::placeholders::_1));

  gps_sub = nh_->create_subscription<nav_msgs::msg::Odometry>(
    "Real_pose",
    1,
    std::bind(&OdometryErrors::gpsCallback, this, std::placeholders::_1));  

  cont_publisher_ = this->create_publisher<nav_msgs::msg::Odometry>("cont_odometry_errors", 10);
  disc_publisher_ = this->create_publisher<nav_msgs::msg::Odometry>("discr_odometry_errors", 10);
  gps_publisher_ = this->create_publisher<nav_msgs::msg::Odometry>("corrupted_gps", 10);
  
  timer_ = this->create_wall_timer(
  500ms, std::bind(&OdometryErrors::timer_callback, this));
}

double OdometryErrors::getYaw(double x, double y, double z, double w)
{
  tf2::Quaternion q(x, y, z, w);
  double siny_cosp = 2 * (q.w ()* q.z() + q.x() * q.y());
  double cosy_cosp = 1 - 2 * (q.y() * q.y() + q.z() * q.z());

  return std::atan2(siny_cosp, cosy_cosp);
}

void OdometryErrors::contCallback(const nav_msgs::msg::Odometry::SharedPtr odom) 
{
    poseCont[0] = odom->pose.pose.position.x;
    poseCont[1] = odom->pose.pose.position.y;

    poseCont[2] = (odom->pose.pose.orientation.x, odom->pose.pose.orientation.y, odom->pose.pose.orientation.z, odom->pose.pose.orientation.w);;

    time = odom->header.stamp;
}

void OdometryErrors::gpsCallback(const nav_msgs::msg::Odometry::SharedPtr odom) 
{
  //Gaussian noise -> 0 mean 0.1 stdd
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<double> d_m(0, 1); //error in position [m]
  std::normal_distribution<double> d_r(0, 0.001); //error in orientation [rad]

  realPose[0] = odom->pose.pose.position.x;
  realPose[1] = odom->pose.pose.position.y;
  realPose[2] = (odom->pose.pose.orientation.x, odom->pose.pose.orientation.y, odom->pose.pose.orientation.z, odom->pose.pose.orientation.w); 

  odom->pose.pose.position.x += d_m(gen);
  odom->pose.pose.position.y += d_m(gen);
  odom->pose.pose.orientation.z += 0;

  odom->pose.covariance[0] = 1;
  odom->pose.covariance[7] = 1;
  odom->pose.covariance[14] = 1;
  odom->pose.covariance[21] = 1e-3;
  odom->pose.covariance[28] = 1e-3;
  odom->pose.covariance[35] = 1e-3;

  odom->twist.covariance[0] = 1e-3;
  odom->twist.covariance[7] = 1e-3;
  odom->twist.covariance[14] = 1e-3;
  odom->twist.covariance[21] = 1e-3;
  odom->twist.covariance[28] = 1e-3;
  odom->twist.covariance[35] = 1e-3;

  gps_publisher_->publish(*odom);
}

void OdometryErrors::discrCallback(const nav_msgs::msg::Odometry::SharedPtr odom) 
{
  poseDisc[0] = odom->pose.pose.position.x;
  poseDisc[1] = odom->pose.pose.position.y;

  poseDisc[2] = (odom->pose.pose.orientation.x, odom->pose.pose.orientation.y, odom->pose.pose.orientation.z, odom->pose.pose.orientation.w);;
  time = odom->header.stamp;
}

void OdometryErrors::publish(float * pose, std::string type)
{
  auto message = std::make_unique<nav_msgs::msg::Odometry>();
  message->pose.pose.position.x = abs(realPose[0] - pose[0]);
  message->pose.pose.position.y = abs(realPose[1] - pose[1]);
  message->pose.pose.orientation.z = abs(realPose[2] - pose[2]);
  message->header.stamp = time;

  if (type == "disc")
  {
    disc_publisher_->publish(std::move(message));
  } else if (type == "cont")
  {
    cont_publisher_->publish(std::move(message));
  }  
}

void OdometryErrors::timer_callback()
{
  publish(poseCont, "cont");
  publish(poseDisc, "disc");
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OdometryErrors>());
  rclcpp::shutdown();

  return 0;
}