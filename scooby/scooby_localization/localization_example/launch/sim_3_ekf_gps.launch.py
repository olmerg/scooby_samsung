# Copyright 2020 Giovani Bernardes
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch Snoopy Gazebo Simulation and localization nodes """

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
import launch
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')
    pkg_scooby_gazebo = get_package_share_directory('scooby_gazebo')
    scooby_description_pkg=get_package_share_directory('snoopy_v1_description')
    localization_node_pkg=get_package_share_directory('localization_example')
    # scooby_bags_pkg = get_package_share_directory('bags')
    
    use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')
    use_rviz = LaunchConfiguration('use_rviz')
    use_teleop = LaunchConfiguration('use_teleop')
    urdf_file= LaunchConfiguration('urdf_file')

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py'),
        ),
        condition=(IfCondition(LaunchConfiguration('verbose')))
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'use_sim_time': True}],
        arguments=[urdf_file]
        )

    start_scooby_teleop_cmd = Node(
        condition=IfCondition(use_teleop),
        package='scooby_teleop',
        executable='scooby_teleop',
        name='scooby_teleop',
        remappings=[
            ("/scooby/cmd_vel", "/cmd_vel")
        ],
        parameters=[{'use_sim_time': True}],
        )

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', os.path.join(localization_node_pkg, 'rviz', 'gps.rviz')],
        condition=IfCondition(use_rviz)
    )
    
    odom_tf = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node_odom',
            output='screen',
            parameters=[os.path.join(localization_node_pkg, 'params', 'ekf_continuous.yaml'),
          {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'odometry/cont_filtered')]
           )
           
    map_tf = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node_map',
            output='screen',
            parameters=[os.path.join(localization_node_pkg, 'params', 'ekf_discrete.yaml'),
            {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'odometry/discr_filtered')]
           )

    sim_gps = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node_gps',
            output='screen',
            parameters=[os.path.join(localization_node_pkg, 'params', 'sim_gps.yaml'),
            {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'Real_pose')]
           )
     
    odom_node = Node(
        package='localization_example',
        executable='odom_exec',
        name='odometry_node',
        output='screen',
        parameters=[os.path.join(localization_node_pkg, 'params', 'odometry.yaml')]
        )

    errors_node = Node(
        package='localization_example',
        executable='errors_exec',
        name='odometry_errors_node',
        output='screen',
        parameters=[{'use_sim_time': True}]
        )
        
    return LaunchDescription([
        DeclareLaunchArgument(
          'world',
          default_value=[os.path.join(pkg_scooby_gazebo, 'worlds', 'snoopy_v1_factory_v1.world'), ''],
          description='SDF world file'),
        DeclareLaunchArgument('verbose', default_value='true',
                              description='Set verbose to "true" to run gzserver.'),
        DeclareLaunchArgument('init', default_value='true',
                              description='Set init to "false" to run gzserver.'),
        DeclareLaunchArgument('factory', default_value='false',
                              description='Set factory to "false" to run gzserver.'),
        DeclareLaunchArgument('force_system', default_value='false',
                              description='Set force_system to "false" to run gzserver.'),

        DeclareLaunchArgument('use_rviz', default_value='true',
                              description='Open RViz.'),
        DeclareLaunchArgument('use_teleop', default_value='true',
                              description='Open scooby_teleop'),
        DeclareLaunchArgument('urdf_file',default_value=os.path.join(scooby_description_pkg, 'urdf', 'snoopy.urdf'),
                              description='urddeclare_urdf_cmd =f file complete path'), 
        
        # launch.actions.ExecuteProcess(
        #     cmd=['ros2', 'bag', 'play', os.path.join(scooby_bags_pkg, 'bags', 'commands', 'commands_0.db3')],
        #     output='screen'
        # ),
        gazebo,
        map_tf,
        odom_tf,
        start_scooby_teleop_cmd,
        robot_state_publisher,
        sim_gps,
        rviz,
        odom_node,
        errors_node
    ])

