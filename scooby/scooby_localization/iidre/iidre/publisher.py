#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
import time, serial
from tf2_ros import TransformBroadcaster
from nav_msgs.msg import Odometry
import geometry_msgs.msg

class UwbXyzPublisher(Node):
    def __init__(self):
        super().__init__('iidre_node')
        self.tfb = TransformBroadcaster(self)
        self.serial = None
        self.device_name = 'uwb'
        self.device_port = '/dev/ttyACM2'
        self.device_frame_id = 'iidre_map'
        self.publish_anchors = True
        
        self.odom = Odometry()
        self.odom.header.frame_id = self.device_frame_id 
        self.odom.child_frame_id = self.device_name  
        self.publisher_ = self.create_publisher(Odometry, '/iidre_pose', 1000)

        self.connect()
        timer_period = 0.02  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
    
    def connect(self):
        if self.serial is not None:
            try:
                self.serial.close()
            except:
                pass
        
        self.get_logger().info("Connecting to {}...".format(self.device_port))
        self.serial = serial.Serial(self.device_port)
        self.get_logger().info("Connected! Now publishing tf frame '{}' in frame '{}'...".format(self.device_name, self. device_frame_id))

    def timer_callback(self):
        try:
            line = self.serial.readline().decode("ascii")
            self.serial.flushInput()
            #self.get_logger().info("Line: {}". format(line))
            self.parse_and_publish(line)
        except (ValueError, IndexError):
            # Ignore the frame in case of any parsing error
            self.get_logger().info("Error when parsing a frame from serial")
        except serial.serialutil.SerialException:
            self.get_logger().info("Device disconnection, retrying ...")
            self.connect()
                

    def parse_and_publish(self, line):
        fb = line.split(":")
        fb_cmd = fb[0]
        fb_data = fb[1].replace("\r\n","").split(",")
        time = fb_data[0]
        
        if fb_cmd == "+DIST":
            # This is usable even if the device has not been preconfigured with the uwbSupervisor
            # Just triangulate the distance (not done here)
            anchor_id = fb_data[1]
            anchor_dist = fb_data[2]
            anchor_xyz = fb_data[3:6]
            ax_m, ay_m, az_m = map(lambda x: float(x)/100, anchor_xyz)

            if self.publish_anchors:
                #self.get_logger().info("Publishing +DIST")

                t = geometry_msgs.msg.TransformStamped()
                t.header.stamp = self.get_clock().now().to_msg()
                t.header.frame_id = self.device_frame_id
                t.child_frame_id = anchor_id
                t.transform.translation.x =  float(ax_m)
                t.transform.translation.y = float(ay_m)
                t.transform.translation.z = float(az_m)
                t.transform.rotation.x = float(fb_data[7]) #FP_PWR_LVL: first-path (*1000) power in dBm
                t.transform.rotation.y = float(fb_data[8]) #IDIFF: LOS/NLOS communication indicator, w/ unit
                t.transform.rotation.z = 0.0
                t.transform.rotation.w = 1.0
                self.tfb.sendTransform(t)   
        
        elif fb_cmd == "+MPOS":
            # This is usable if device has been preconfigured with the uwbSupervisor
            x, y, z = fb_data[1:4]
            # Convert from centimeters (in the JSON infra file) to meters
            x_m, y_m = map(lambda x: float(x)/100, [x, y])

            if self.publish_anchors:
                #self.get_logger().info("Publishing +MPOS")
                t = geometry_msgs.msg.TransformStamped()
                t.header.stamp = self.get_clock().now().to_msg()
                t.header.frame_id = self.device_frame_id
                t.child_frame_id = self.device_name
                t.transform.translation.x =  float(x_m)
                t.transform.translation.y = float(y_m)
                t.transform.translation.z = 0.0
                t.transform.rotation.x = 0.0
                t.transform.rotation.y = 0.0
                t.transform.rotation.z = 0.0
                t.transform.rotation.w = 1.0
                self.tfb.sendTransform(t)   

                #Also publishes a odom message
                self.odom.header.stamp = t.header.stamp
                self.odom.pose.pose.position.x = float(x_m)
                self.odom.pose.pose.position.y = float(y_m)
                self.publisher_.publish(self.odom)
        


def main(args=None):
    rclpy.init(args=args)

    uwbPub = UwbXyzPublisher()
    rclpy.spin(uwbPub)

    #uwbPub.run()

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    uwbPub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

