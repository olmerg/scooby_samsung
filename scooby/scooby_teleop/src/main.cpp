// Copyright 2020 Giovani Bernardes.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QApplication>
#include <iostream>
#include <rclcpp/rclcpp.hpp>
#include "scooby_teleop.h"

class TeleopApp : public QApplication
{
public:
  rclcpp::Node::SharedPtr nh_;

  explicit TeleopApp(int& argc, char** argv)
    : QApplication(argc, argv)
  {
    rclcpp::init(argc, argv);
    nh_ = rclcpp::Node::make_shared("Scooby");
  }

  ~TeleopApp()
  {
    rclcpp::shutdown();
  }

  int exec()
  {
    scooby_teleop teleop(nh_);
    teleop.show();

    return QApplication::exec();
  }
};


int main(int argc, char * argv[])
{

  TeleopApp app(argc, argv);
  return app.exec();
}
