// Copyright 2020 Giovani Bernardes.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SCOOBY_TELEOP_H
#define SCOOBY_TELEOP_H


#include <QMainWindow>

namespace Ui {
class scooby_teleop;
}

#include <geometry_msgs/msg/twist.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/laser_scan.hpp>

#include <memory>
#include <utility>
#include <QTimer>

class scooby_teleop : public QMainWindow                      
{
  Q_OBJECT
public:
  /// \brief Teleoperation node that publishes
  /// velocity commands based on key buttons.
  explicit scooby_teleop(rclcpp::Node::SharedPtr& nh, QWidget *parent = 0);
  ~scooby_teleop();


  rclcpp::Node::SharedPtr nh_;
 
private slots:
  void onUpdate();
  
private:

  Ui::scooby_teleop *ui;

  QTimer* update_timer_;
  
  float idx,idy;
  
  void SendCommand();
  
  /// \brief Laser messages subscriber
  //rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr laser_sub_;

  /// \brief Velocity command publisher
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr cmd_pub_;

  /// \brief Minimum allowed distance from target
  double min_dist_ = 1.0;

  /// \brief Scale linear velocity, chosen by trial and error
  double linear_k_ = 0.02;

  /// \brief Scale angular velocity, chosen by trial and error
  double angular_k_ = 0.08;
};

#endif // SCOOBY_TELEOP_H
