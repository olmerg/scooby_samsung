/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#ifndef SCOOBY_NODE_SENSORS_JOINT_STATE_HPP_
#define SCOOBY_NODE_SENSORS_JOINT_STATE_HPP_

#include <sensor_msgs/msg/joint_state.hpp>

#include "scooby_node/sensors/sensors.hpp"

namespace lma
{
namespace scooby
{
namespace sensors
{
constexpr uint8_t JOINT_NUM = 2;

// 
//constexpr double RPM_TO_MS = 0.229 * 0.0034557519189487725;

// 
//constexpr double TICK_TO_RAD = 0.001533981;

class JointState : public Sensors
{
 public:
  explicit JointState(
    std::shared_ptr<rclcpp::Node> & nh,
    const std::string & topic_name = "joint_states",
    const std::string & frame_id = "Base_Link");

  void publish(
    const rclcpp::Time & now,
    std::shared_ptr<SerialWrapper> & serial_wrapper) override;

 private:
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr pub_;
};
} // sensors
} // scooby
} // lma
#endif // scooby_NODE_SENSORS_JOINT_STATE_HPP_
