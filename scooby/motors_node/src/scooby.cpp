/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#include "scooby_node/scooby.hpp"
#include <string>

using namespace lma;
using namespace std::chrono_literals;
using namespace scooby;

Scooby::Scooby(const std::string & ip_port)
: Node("scooby_node",rclcpp::NodeOptions().use_intra_process_comms(true))
{
  RCLCPP_INFO(get_logger(), "Init Scooby Node Main");
  node_handle_ = std::shared_ptr<::rclcpp::Node>(this, [](::rclcpp::Node *) {});

  

  add_motors();
  add_wheels();
  //init_serial_wrapper(ip_port);

  //add_sensors();
  add_devices();

  run();
}

Scooby::Wheels* Scooby::get_wheels()
{
  return &wheels_;
}

Scooby::Motors* Scooby::get_motors()
{
  return &motors_;
}

void Scooby::init_serial_wrapper(const std::string & device1)
{

  

  this->declare_parameter("serial.port1");
  this->declare_parameter("serial.port2");
  this->declare_parameter("serial.protocol_version");

  std::string p1="";
  int baudrate=0;
  this->get_parameter_or<std::string>("serial.port1", p1,std::string("/dev/ttyACM0"));
  this->get_parameter_or<int>("serial.baudrate", baudrate, 115200);
  
  serial_wrapper_ = std::make_shared<SerialWrapper>(p1,baudrate);

  RCLCPP_INFO(this->get_logger(), "Init SerialWrapper");

  


}



void Scooby::add_motors()
{
  RCLCPP_INFO(this->get_logger(), "Add Motors");

  this->declare_parameter("motors.profile_acceleration_constant");
  this->declare_parameter("motors.profile_acceleration");

  this->get_parameter_or<float>(
    "motors.profile_acceleration_constant",
    motors_.profile_acceleration_constant,
    214.577);

  this->get_parameter_or<float>(
    "motors.profile_acceleration",
    motors_.profile_acceleration,
    0.0);
}

void Scooby::add_wheels()
{
  RCLCPP_INFO(this->get_logger(), "Add Wheels");

  this->declare_parameter("wheels.separation");
  this->declare_parameter("wheels.radius");

  this->get_parameter_or<float>("wheels.separation", wheels_.separation, 0.400);
  this->get_parameter_or<float>("wheels.radius", wheels_.radius, 0.1);
}

void Scooby::add_sensors()
{
  RCLCPP_INFO(this->get_logger(), "Add Sensors");

  this->declare_parameter("sensors.bumper_1");
  this->declare_parameter("sensors.bumper_2");

  this->declare_parameter("sensors.illumination");

  this->declare_parameter("sensors.ir");

  this->declare_parameter("sensors.sonar");

  bool is_connected_bumper_1; 
  this->get_parameter_or<bool>("sensors.bumper_1",is_connected_bumper_1,false);
  bool is_connected_bumper_2 ; 
  this->get_parameter_or<bool>("sensors.bumper_2",is_connected_bumper_2,false);

  bool is_connected_illumination;
  this->get_parameter_or<bool>("sensors.illumination",is_connected_illumination,false);

  bool is_connected_ir;
  this->get_parameter_or<bool>("sensors.ir",is_connected_ir,false);

  bool is_connected_sonar;
  this->get_parameter_or<bool>("sensors.sonar",is_connected_sonar,false);

  sensors_.push_back(new sensors::BatteryState(
    node_handle_,
    "battery_state"));

  // sensors_.push_back(new sensors::Imu(
  //   node_handle_,
  //   "imu",
  //   "magnetic_field",
  //   "IMU_F_Link"));

  sensors_.push_back(new sensors::SensorState(
    node_handle_,
    "sensor_state",
    is_connected_bumper_1,
    is_connected_bumper_2,
    is_connected_illumination,
    is_connected_ir,
    is_connected_sonar));

  sensors_.push_back(new sensors::JointState(node_handle_, "joint_states", "Base_Link"));
}

void Scooby::add_devices()
{
  RCLCPP_INFO(this->get_logger(), "Add Devices");
  devices_["motor_power"] =
    new devices::MotorPower(node_handle_, serial_wrapper_, "motor_power");
  devices_["reset"] =
    new devices::Reset(node_handle_, serial_wrapper_, "reset");
}

void Scooby::run()
{
  RCLCPP_INFO(this->get_logger(), "Run!");

  publish_timer(std::chrono::milliseconds(20));

  parameter_event_callback();
  cmd_vel_callback();
}

void Scooby::publish_timer(const std::chrono::milliseconds timeout)
{
  publish_timer_ = this->create_wall_timer(
    timeout,
    [this]() -> void
      {
        rclcpp::Time now = this->now();

        bool result=serial_wrapper_->refresh(now.seconds(),wheels_.separation,wheels_.radius);

        for (const auto &sensor:sensors_)
        {
          sensor->publish(now, serial_wrapper_);
        }
      }
    );
}




void Scooby::parameter_event_callback()
{
  priv_parameters_client_ = std::make_shared<rclcpp::AsyncParametersClient>(this);
  while (!priv_parameters_client_->wait_for_service(std::chrono::seconds(1)))
  {
    if (!rclcpp::ok())
    {
      RCLCPP_ERROR(this->get_logger(), "Interrupted while waiting for the service. Exiting.");
      return;
    }

    RCLCPP_WARN(this->get_logger(), "service not available, waiting again...");
  }

  auto param_event_callback =
    [this](const rcl_interfaces::msg::ParameterEvent::SharedPtr event) -> void
  {
    for (const auto & changed_parameter : event->changed_parameters)
    {
      RCLCPP_DEBUG(
        this->get_logger(),
        "changed parameter name : %s",
        changed_parameter.name.c_str());

      
    }
  };

  parameter_event_sub_ = priv_parameters_client_->on_parameter_event(param_event_callback);
}

void Scooby::cmd_vel_callback()
{
  auto qos = rclcpp::QoS(rclcpp::KeepLast(3));
  cmd_vel_sub_ = this->create_subscription<geometry_msgs::msg::Twist>(
    "cmd_vel",
    qos,
    [this](const geometry_msgs::msg::Twist::SharedPtr msg) -> void
      {
        serial_wrapper_->setSpeed(msg->linear.x, msg->angular.z);
       RCLCPP_DEBUG(
          this->get_logger(),
          "lin_vel: %f ang_vel: %f msg : %s", msg->linear.x, msg->angular.z, "error:");
      // RCLCPP_INFO(
      //     this->get_logger(),
      //     "lin_vel: %f ang_vel: %f msg : %s", msg->linear.x, msg->angular.z, "error:");
       }
    );
}
