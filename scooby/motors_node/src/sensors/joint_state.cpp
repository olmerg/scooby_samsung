/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#include <array>

#include "scooby_node/sensors/joint_state.hpp"

using namespace lma;
using namespace scooby;

sensors::JointState::JointState(
  std::shared_ptr<rclcpp::Node> & nh,
  const std::string & topic_name,
  const std::string & frame_id)
: Sensors(nh, frame_id)
{
  pub_ = nh->create_publisher<sensor_msgs::msg::JointState>(topic_name, this->qos_);

  RCLCPP_INFO(nh_->get_logger(), "Succeeded to create joint state publisher");
}

void sensors::JointState::publish(
  const rclcpp::Time & now,
  std::shared_ptr<SerialWrapper> & serial_wrapper)
{
  auto msg = std::make_unique<sensor_msgs::msg::JointState>();

  static std::array<double, JOINT_NUM> last_diff_position, last_position;

    
  std::array<double, JOINT_NUM> position =
    {serial_wrapper->theta_l,serial_wrapper->theta_r};

  std::array<double, JOINT_NUM> velocity =
    {serial_wrapper->w_l,serial_wrapper->w_r};

  // std::array<int32_t, JOINT_NUM> current =
  //   {0,
  //   0};

  msg->header.frame_id = this->frame_id_;
  msg->header.stamp = now;

  msg->name.push_back("Roda_L_Joint");
  msg->name.push_back("Roda_R_Joint");

  msg->position.push_back(position[0]);
  msg->position.push_back(position[1]);

  msg->velocity.push_back(velocity[0]);
  msg->velocity.push_back(velocity[1]);

  // msg->effort.push_back(current[0]);
  // msg->effort.push_back(current[1]);

  //last_diff_position[0] += (position[0] - last_position[0]);
  //last_diff_position[1] += (position[1] - last_position[1]);

 // last_position = position;

  pub_->publish(std::move(msg));
}
