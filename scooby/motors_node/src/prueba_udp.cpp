#include <string>
#include <iostream>

#include "../include/scooby_node/udp_socket.hpp"
#include "../include/scooby_node/protocoloc.h"

/***
 *Instruction to test
 g++ prueba_udp.cpp  ../include/scooby_node/udp_socket.hpp -o primero -lboost_system -lboost_date_time -lpthread 
 */

  int main(int argc, char * argv[])
  {
    lma::scooby::udp_socket _udp_socket;
    std::vector<unsigned char> _buffer_tosend;
    std::vector<unsigned char> _buffer_received;
    

    _buffer_tosend.reserve(16);
    _buffer_tosend.resize(16,0);

    _buffer_received.reserve(56);
    _buffer_received.resize(56,0);


    _udp_socket.configure(25000,25001,"192.168.0.7",10);


    std::string Message;
    std::vector<double> input;
    input.push_back(2.0);
    input.push_back(1.9);

    //encode
    int  typei[]={9,9};
    maencode(&_buffer_tosend[0],&input[0],typei,input.size(),BIGENDIAN);
    
    auto bytes_sent=_udp_socket.send(&_buffer_tosend[0],_buffer_tosend.size(),Message);
    std::cout<<"error to sent? "<<Message<<"\n";
    auto bytes_recvd=_udp_socket.receive(&_buffer_received[0],Message);
    std::cout<<"error to receive? "<<Message<<"\n";
    //decode
    std::vector<double> output;
    
    int  tipe[]={9,9,9,9,9,9,9};
    output.resize(sizeof(tipe)/4,0);
    madecode(&output[0],&_buffer_received[0],tipe,sizeof(tipe)/4,BIGENDIAN);

    

    //just printing
    
    std::cout<<"sent "<<bytes_sent<<"\n";
    std::cout<<"received "<<bytes_recvd<<"\n";

//for (int i=0;i<buffer_received.size()-1;i++)
//               std::cout<<(int)buffer_received[i]<<",";
std::cout<<"\n";                
for (int i=0;i<output.size();i++)
                std::cout<<output[i]<<",";
    std::cout<<"\n"<<"Hola Scooby"<<"\n";
    return 0;
  }