#!/usr/bin/env python3
#
# Copyright 2021 UNICAMP.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Authors: Giovani Bernardes

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

# from ament_index_python.packages import get_package_share_directory
# from launch import LaunchDescription
# from launch.actions import DeclareLaunchArgument
# from launch.actions import IncludeLaunchDescription
# from launch.launch_description_sources import PythonLaunchDescriptionSource
# from launch.substitutions import LaunchConfiguration
# from launch.substitutions import ThisLaunchFileDir
# from launch_ros.actions import Node


def generate_launch_description():

    imu_param_dir = LaunchConfiguration(
        'imu_param_dir',
        default=os.path.join(
            get_package_share_directory('xsens_mti_driver'),
            'param',
            'xsens_mti_node_ROS2.yaml'))


    return LaunchDescription([

        DeclareLaunchArgument(
            'imu_param_dir',
            default_value=imu_param_dir,
            description='Full path to xsens_mti_driver parameter file to load'),


        Node(
            package='xsens_mti_driver',
            executable='xsens_mti_node',
            parameters=[imu_param_dir],
            #arguments=['-i', usb_port],
            output='screen',
            remappings=[("imu/data", "imu"), ('imu/time_ref', 'clock')]
        )
    ])
