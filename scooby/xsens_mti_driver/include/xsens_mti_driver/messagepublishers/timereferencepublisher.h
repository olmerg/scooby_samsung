
//  Copyright (c) 2003-2020 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//

#ifndef TIMEREFERENCEPUBLISHER_H
#define TIMEREFERENCEPUBLISHER_H

#include "packetcallback.h"
#include <sensor_msgs/msg/time_reference.hpp>

#include <rosgraph_msgs/msg/clock.hpp>

struct TimeReferencePublisher : public PacketCallback
{
    //rclcpp::Publisher<sensor_msgs::msg::TimeReference>::SharedPtr pub;
    rclcpp::Publisher<rosgraph_msgs::msg::Clock>::SharedPtr pub;
    
    TimeReferencePublisher(std::shared_ptr<rclcpp::Node> &node)
    {
        node->get_parameter_or<int>( "publisher_queue_size", pub_queue_size, 5);  
        auto qos = rclcpp::QoS(rclcpp::KeepLast(pub_queue_size));
  	    //pub = node->create_publisher<sensor_msgs::msg::TimeReference>("imu/time_ref", qos);
              pub = node->create_publisher<rosgraph_msgs::msg::Clock>("/clock", qos.transient_local());
    }

    void operator()(const XsDataPacket &packet, rclcpp::Time timestamp)
    {
        if (packet.containsSampleTimeFine())
        {
            const uint32_t SAMPLE_TIME_FINE_HZ = 10000UL;
            const uint32_t ONE_GHZ = 1000000000UL;
            uint32_t sec, nsec, t_fine;

	    //sensor_msgs::msg::TimeReference msg;
	    rosgraph_msgs::msg::Clock clock;

            t_fine = packet.sampleTimeFine();
            sec = t_fine / SAMPLE_TIME_FINE_HZ;
            nsec = (t_fine % SAMPLE_TIME_FINE_HZ) * (ONE_GHZ / SAMPLE_TIME_FINE_HZ);

            if (packet.containsSampleTimeCoarse())
            {
                sec = packet.sampleTimeCoarse();
            }

            rclcpp::Time sample_time(sec, nsec);

            //msg.header.stamp = timestamp;
            //// msg.header.frame_id = unused
            //msg.time_ref = sample_time;
            //// msg.source = optional
	    //pub->publish(msg);
	    
	    //clock.header.stamp = timestamp;
	    clock.clock = sample_time;
	    pub->publish(clock);
        }
    }
};

#endif
